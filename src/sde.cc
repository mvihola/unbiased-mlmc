
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <random>
#include <cassert>

#include "types.hh"
#include "sde.hh"
#include "helpers.hh"

using namespace std;

// Only allocate some space to BM
sde::sde(target_func f_, const vector<double> &x0_, 
         double T_, int base_, const vector<int> &active_levels_, 
         int max_level_) : 
         T{T_}, x0{x0_}, base{base_}, active_levels{active_levels_},
         max_level{max_level_} { /*{{{*/
  DEBUG("Creating sde.")
  f = f_;
  d = x0.size();
  // Allocate tmp variable for level_diff
  y_ = new double[f.d];
  // Do not allocate any yet
  nalloc = -1;
  // Initialise driver
  normal_distribution<double> randn(0.0, 1.0);
  
}
/*}}}*/

// Only delete some space to BM
sde::~sde() { /*{{{*/
  delete[] y_;
  if (nalloc >= 0) delete[] B;
  DEBUG("Deleted sde.")
}
/*}}}*/

// The base_n function will be the same in all
uint64_t sde::base_n(int p) { /*{{{*/
  uint64_t i = 1;
  if (base == 2) {
    // Do fast bitwise shift left exponentiation in this common case
    i <<= p;
  } else {
    for (int j=1; j<=p; j++) i *= base;
  }
  return i;
}
/*}}}*/

int sde::determine_level(int i) { /*{{{*/
  int c_level;
  
  if (i < 0) {
    c_level = -1;
  } else if (i < (int)active_levels.size()) {
    c_level = active_levels[i];
  } else { // i >= active_levels.size()
    int last = (active_levels.size()>0) ? active_levels.back() : -1;
    c_level = last + (i+1 - active_levels.size());
  }
  
  if (max_level >= 0) {
    c_level = min(c_level, max_level);
  }
  return c_level;
} /*}}}*/

void sde::simulate_bm(int n_, PRNG &g) { /*{{{*/
  int n = determine_level(n_);
  int i, j, bn = base_n(n);
  n_bm = n;
  double sqrtdt = sqrt(T/double(bn));
  if (nalloc<n) {
    // Allocate more space
    if (nalloc >= 0) delete [] B;
    nalloc = n;
    try {
      B = new double[d*(bn+1)];
    } catch(int e) {
      error("Failed to allocate "
            + to_string(d*(bn+1)*sizeof(double)) 
            + " bytes for Brownian path; exiting.");
    }
  }
  // Initialise
  for (j=0; j<d; j++) B[j] = 0.0;
  // Then do BM path simulation
  for (i=1; i<=bn; i++) {
    for (j=0; j<d; j++) {
      B[j + d*i] = B[j + d*(i-1)] + sqrtdt*randn(g);
    }
  }
}
/*}}}*/

void sde::level_diff(double* y, int n_, int m_) { /*{{{*/
  int n = determine_level(n_);
  int m = determine_level(m_);
  //double y_[f.d];
  unsigned int i;
  
  // If same level (in practice, beyond max_level), return zero
  if (n == m) {
    for (i=0; i<f.d; i++) y[i] = 0;
    return;
  }
  
  // Calculate levels
  if (n >= 0) {
    level(y, n);
    if (m >= 0) {
      // Both calculated, so calculate y_n - y_m
      level(y_, m);
      for (i=0; i<f.d; i++) y[i] -= y_[i];
    }
    // Otherwise leve as y_n - 0 = y_n
  } else {
    // The result is just 0 - y_m.
    level(y_, m);
    for (i=0; i<f.d; i++) y[i] = -y_[i];
  }
}
/*}}}*/

uint64_t sde::cost(int n_, int m_) { /*{{{*/
  int n = determine_level(n_);
  int m = determine_level(m_);
  int c = 0;
  if (n != m) {
    if (n>=0) c += base_n(n);
    if (m>=0) c += base_n(m);
  }
  return c;
}
/*}}}*/

