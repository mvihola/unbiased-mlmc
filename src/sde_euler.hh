/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SDE_EULER_HH_
#define _SDE_EULER_HH_

#include <vector>
#include <random>
#include "sde.hh"

using namespace std;

/**
 * SDE class with Euler discretisation.
 */
class sde_euler : public sde {
 private:
   // Space for temporary variables
   double *x_, *x, *mu_x, *sigma_x, *dB;

 public:
   
   // Calculate simple euler discretisation
   void level(double* y, int n);
   
   // Default constructor: Use default values
   sde_euler(param_func mu, param_func sigma, target_func f,
       const vector<double> &x0 = vector<double>(1, 0.0), 
       double T = 1.0, 
       int base = 2, const vector<int> &active_levels = vector<int>(),
       int max_level = -1);
   
   // Copy constructor
   sde_euler(const sde_euler &other) :
   sde_euler(other.mu, other.sigma, other.f, other.x0, other.T, 
             other.base, other.active_levels, other.max_level) {};
   
   simulator* clone();
   ~sde_euler();
   
};


#endif
