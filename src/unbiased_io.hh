/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _UNBIASED_IO_HH_
#define _UNBIASED_IO_HH_

#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <list>
#include <mutex>
#include <string>

#include "types.hh"
#include "helpers.hh"

using namespace std;

/**
 * Data record of one (unbiased) estimator, to be written in 
 * the output file, or aggregated to an average estimator.
 */
class data_item { /*{{{*/
 private:
   // These are used by the cumulative estimate
   static uint64_t n_records;
   static vector<double> mean_est;
   static vector<double> mean_var;
   static vector<double> emp_var; 
   static mutex guard;

 public:
  vector<double> m;
  vector<double> v;
  uint64_t c;
  void write_header(ostream&);
  void write_ascii_record(ostream&);
  void write_binary_record(ostream&);
   // This is called for same data that is written
  void add_hook();
  void write_hook() {}; 
  static void finalise_hook();
  static string summary();
  static bool tol_ok(vector<double>& tol);
  static string status();
};
/*}}}*/

/**
 * One record of statistics related to an unbiased estimator,
 * to be written to a stats file.
 */
class stats_item { /*{{{*/
 private:
   static unsigned int d; // Dimension 
   static uint64_t n_records; // Number of records
   static vector<vector<double>> var_est;
   static vector<vector<double>> mean_est;
   static vector<uint64_t> n_tot;
   static mutex guard;
 public:
   // level means
   vector<vector<double>> m;
   // level variances
   vector<vector<double>> v;
   // level numbers
   vector<uint64_t> n;
  void write_header(ostream&);
  void write_ascii_record(ostream&);
  void write_binary_record(ostream&);
  void add_hook();
  void write_hook() {};
  static void finalise_hook();
  static void get_est(vector<uint64_t>& n_tot_, vector<vector<double>>& var_est_);
  static string summary();
};
/*}}}*/

/**
 * General thread-safe writer class, where threads can add records to a list,
 * which can be flushed every now and then.
 */
template <class item_type>
class unbiased_io { /*{{{*/
 protected:
   // Lock not to mess up the list
   mutex list_guard;
   // The binary mode; 'true' if so
   bool binary_mode;
   // Call the provided
   // add_hook when adding,
   // write_hook when writing and 
   // finalise_hook when finishing
   bool call_hooks;
   // The out stream
   ostream* os;
   // The out file
   ofstream of;
   // List where items are added
   list<item_type> buf;
   // Whether header has been written
   bool header_written;
   int write_header();
   void write_record(item_type&);
 public:
   bool active;
   unbiased_io(string fname = string("-"), bool binary = false, 
               bool call_hooks = false);
   ~unbiased_io();
   void add(item_type&);
   void flush();
};
/*}}}*/


template<class item_type>
unbiased_io<item_type>::unbiased_io(string fname, bool binary, bool call_hooks_) 
: binary_mode{binary}, call_hooks{call_hooks_} { /*{{{*/
  active = true;
  DEBUG("Creating unbiased_io '" + fname + "'.")
  if (fname.empty()) {
    // No output
    active = false;
    return;
  } else if (fname == "-") {
    os = &cout;
  } else {
    // Open file; overwrite. Write files in binary
    if (binary_mode) {
      of.open(fname.c_str(), ios::trunc | ios::binary);
    } else {
      of.open(fname.c_str(), ios::trunc);
    }
    if (!of.good())
      error("Cannot open file '" + fname + "' for writing.");
    os = &of;
  }
  buf.clear();
  header_written = false;
  if (!binary_mode) {
    *os << scientific << setprecision(17);
  }
}
/*}}}*/


template<class item_type>
unbiased_io<item_type>::~unbiased_io() { /*{{{*/
  if (active || call_hooks) {
    flush();
    DEBUG("Closing output file.")
  }
  if (call_hooks) item_type::finalise_hook();
  of.close();
  DEBUG("Deleted unbiased_io.")
}
/*}}}*/


template<class item_type>
void unbiased_io<item_type>::add(item_type& data) { /*{{{*/
  // if (!active && !data.add_h) return;
  DEBUG("Adding data record.")
  list_guard.lock(); // Just not to mess up the list
  buf.push_back(data);
  list_guard.unlock();
  if (call_hooks) data.add_hook();
}
/*}}}*/


template<class item_type>
int unbiased_io<item_type>::write_header() { /*{{{*/
   if (buf.empty()) return 0;
   if (active) {
     item_type first = buf.front();
     first.write_header(*os);
     header_written = true;
   }
   return 1;
}
/*}}}*/


template<class item_type>
void unbiased_io<item_type>::write_record(item_type& item) { /*{{{*/
   if (call_hooks) item.write_hook();
   if (active) {
     if (binary_mode) {
       item.write_binary_record(*os);
     } else {
       item.write_ascii_record(*os);
     }
   }
}
/*}}}*/


template<class item_type>
void unbiased_io<item_type>::flush() { /*{{{*/
  // if (!active) return;
  if (!header_written) {
    if (!write_header()) return;
  }
  if (active || call_hooks) {
  DEBUG("Flushing output.")
  bool empty = false;
  item_type data;
  while (!empty) {
    list_guard.lock();
    empty = buf.empty();
    if (!empty) {
      data = buf.front();
      buf.pop_front();
    }
    list_guard.unlock();
    if (!empty) write_record(data);
  }
   }
}
/*}}}*/

#endif
