/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _HELPERS_HH_
#define _HELPERS_HH_

#include <string>
#include <sstream>
#include <iostream>

using namespace std;

void error(string);

#endif
