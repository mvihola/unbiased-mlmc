/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _TYPES_HH_
#define _TYPES_HH_

#include <random>
#include <string>
#include <vector>
#include <cstdint>

#ifdef _DEBUG_
#define DEBUG(x) std::cerr << x << std::endl;
#else
#define DEBUG(x) 
#endif

using namespace std;

// The type of pseudo-random number generator
typedef std::mt19937_64 PRNG;

// Type of the terminal value function which is to be estimated
struct target_func {
  void (*func)(const double*, double*);
  unsigned int d;
};


// Type of 'parameter function', that is, parameters of the sde
typedef void (*param_func)(const double*, double, double*);

// Type of the SDE functional
typedef void (*functional)(const double*, double*);

struct param {
  string config_file;
  string inline_config;
  string model_lib;
  string out_file;
  string stats_file;
  string sde_scheme;
  string sampling_scheme;
  param_func mu;
  param_func sigma;
  param_func dsigma;
  functional func;
  unsigned int d_f;
  double T;
  uint64_t n_samples;
  uint64_t batch;
  double tail_prob;
  double gamma;
  vector<double> tol;
  unsigned int n_threads;
  unsigned int base;
  bool binary;
  bool coupled;
  bool calc_vars;
  bool sum;
  bool summary;
  bool optimise;
  bool verbose;
  int max_level;
  vector<uint64_t> seed;
  vector<double> x0;
  vector<double> p;
  vector<int> levels;
};

#endif
