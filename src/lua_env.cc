/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "lua_env.hh"
#include <iostream>

using namespace std;

lua_env::lua_env() { /*{{{*/
  // Create new state, and do standard libraries
  L = luaL_newstate();
  luaopen_base(L);
  luaopen_io(L);
  luaopen_string(L);
  luaopen_math(L);
} /*}}}*/

lua_env::~lua_env() { /*{{{*/
  lua_close(L);
} /*}}}*/


void lua_env::do_file(string config_file) { /*{{{*/
  if (luaL_dofile(L, config_file.c_str())) 
      error("Cannot run Lua config file: " + string(lua_tostring(L, -1)));
} /*}}}*/

void lua_env::do_eval(string inline_config) { /*{{{*/
    if (luaL_loadbuffer(L, inline_config.c_str(), 
                        inline_config.length(), "=<inline>") 
        || lua_pcall(L, 0, 0, 0))
      error("Cannot run Lua inline config '" + inline_config + "'");
} /*}}}*/

string lua_env::get_string(string fname, string def) { /*{{{*/
  string v;
  lua_getglobal(L, fname.c_str());
  if (lua_isnil(L, -1))
    return(def);
  if (!lua_isstring(L, -1))
    error("'" + fname + "' should be a string");
  v = string(lua_tostring(L, -1));
  lua_pop(L, 1);
  return v;
} /*}}}*/


