/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SIMULATOR_HH_
#define _SIMULATOR_HH_

#include <vector>
#include <random>
#include <cassert>

#include "types.hh"

using namespace std;


// Generic interface class, which is used by the "unbiased" class.
class simulator {
 public:
   // Default constructor: Use default values as stated above
   simulator() {};
   virtual ~simulator() {};

   // Function which simulates a Brownian path of length n
   virtual void simulate_bm(int n, PRNG& g) = 0;
   
   // Function which calculates a difference of y = Y_n - Y_m 
   // based on a Monte Carlo sample.
   virtual void level_diff(double* y, int n, int m) = 0;

   // Function returning the dimension of the simulated variable
   // (or functional).
   virtual int get_d() = 0;
   
   // Cloning function
   virtual simulator* clone() = 0;
   
   // Function returning the (proportional) cost of single 
   // level_diff(y,n,m,g)
   virtual uint64_t cost(int n, int m) = 0;
};

#endif
