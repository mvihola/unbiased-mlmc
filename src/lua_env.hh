/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _LUA_HELPERS_HH_
#define _LUA_HELPERS_HH_

#include <string>
#include <vector>

#include "helpers.hh"

extern "C" {
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
}

using namespace std;

/**
 * Class which creates a Lua environment, runs a file or evaluates a string,
 * and then hosts a number of helpers to get values from Lua environment.
 */
class lua_env {
 private:
   // The state of the Lua environment
   lua_State* L;
 public:
   lua_env();
   ~lua_env();
   
   // Get string from variable called fname, with default def.
   string get_string(string fname, string def);
   
   // Get number (scalar) from variable called fname, with default def.
   template<typename num_type>
   num_type get_number(string fname, num_type def);
   
   // Get vector (scalar/table) from fname with default def.
   template<typename num_type>
   vector<num_type> get_vector(string fname, 
                          vector<num_type> def);
   
   // Run a file in the Lua environment
   void do_file(string);
   
   // Evaluate a string in the Lua env.
   void do_eval(string);
};

template<typename num_type>
num_type lua_env::get_number(string fname, num_type default_value) { /*{{{*/
  num_type value;
  lua_getglobal(L, fname.c_str());
  if (!lua_isnil(L, -1)) {
    if (!lua_isnumber(L, -1)) 
      error("Error: '" + fname + "' should be a number.");
    value = (num_type)lua_tonumber(L, -1);
  } else {
    value = default_value;
  }
  lua_pop(L, 1);
  return value;
} /*}}}*/

template<typename num_type>
vector<num_type> lua_env::get_vector(string fname, 
                          vector<num_type> default_value) { /*{{{*/
  lua_getglobal(L, fname.c_str());
  if (lua_isnumber(L, -1)) {
    vector<num_type> v(1);
    v[0] = lua_tonumber(L, -1);
    return(v);
  }
  if (lua_isnil(L, -1)) {
    return(default_value);
  }
  if (!lua_istable(L, -1)) error("'" + fname + "' must be a number or table.");
  vector<num_type> v;
  lua_pushnil(L);
  while(lua_next(L, -2)) {
    v.push_back((num_type)lua_tonumber(L, -1));
    lua_pop(L, 1);
  }
  lua_pop(L, 1);
  return v;
} /*}}}*/


#endif
