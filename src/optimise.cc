/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "simulator.hh"
#include "unbiased_io.hh"
#include "optimise.hh"

// Helper which calculates optimal p (unnormalised!) 
// based on a variance estimate
vector<double> optimise::find_p(vector<vector<double>>& var_est, 
                                vector<uint64_t>& n_tot, simulator* sim) { /*{{{*/
  vector<double> tmp(n_tot.size(), 0.0);
  unsigned int i, j, N = n_tot.size();
  for (i=0; i<N; i++) {
    double var_i = 0.0;
    for (j=0; j<var_est[i].size(); j++) var_i += var_est[i][j];
    tmp[i] = sqrt(var_i/double(sim->cost(i, int(i)-1)));
  }
  return tmp;
}
/*}}}*/

void optimise::set_optimal(vector<double>& p, double& tail_prob,
                           simulator* sim) { /*{{{*/
  DEBUG("Finding optimal p")
  vector<vector<double>> var_est;
  vector<uint64_t> n_tot;
  stats_item::get_est(n_tot, var_est);
  vector<double> tmp = find_p(var_est, n_tot, sim);
  
  unsigned int i, N = n_tot.size(),  M = p.size();
  
  // Normalise to probability
  double sum = 0.0;
  for (i=0; i<N; i++) sum += tmp[i];  
  for (i=0; i<N; i++) tmp[i] /= sum;
  
  // Determine tail probability
  sum = 0.0;
  for (i=0; i<M; i++) {
    sum += tmp[i];
    p[i] = (i<N) ? tmp[i] : 0.0;
  }
  tail_prob = 1.0 - sum;  
  
} /*}}}*/

