/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <random>
#include <cstring>
#include <cassert>

#include "sde_milstein.hh"
#include "types.hh"

using namespace std;

sde_milstein::sde_milstein(param_func mu_, param_func sigma_, 
                           param_func dsigma_, target_func f_,
                     const vector<double> &x0_, double T_, int base_, 
                     const vector<int> &active_levels_, int max_level_)
                     : sde(f_, x0_, T_, base_, active_levels_,
                           max_level_) { /*{{{*/
  DEBUG("Creating sde_milstein.")
  mu = mu_;
  sigma = sigma_;
  dsigma = dsigma_;
  
  assert(d==1);
} /*}}}*/


sde_milstein::~sde_milstein() { /*{{{*/
  DEBUG("Deleted sde_milstein.")
}
/*}}}*/

void sde_milstein::level(double* y, int n) { /*{{{*/
  double dB, mu_x[1], sigma_x[1], dsigma_x[1];
  double x = x0[0];
  int nb = base_n(n_bm), ninc = base_n(n_bm-n);
  double t, T_over_nb = T/double(nb);
  double dt = double(ninc)*T_over_nb;
  
  for (int i=0; i<nb; i+=ninc) {
    t = double(i)*T_over_nb;
    dB = B[i+ninc]-B[i];
    mu(&x, t, mu_x);
    sigma(&x, t, sigma_x);
    dsigma(&x, t, dsigma_x);
    x = x + mu_x[0]*dt + sigma_x[0]*dB 
          + 0.5*sigma_x[0]*dsigma_x[0]*(dB*dB-dt);
  }
  f.func(&x, y);
}
/*}}}*/

simulator* sde_milstein::clone() { /*{{{*/
  return static_cast<simulator*>(new sde_milstein(*this));
} /*}}}*/

