/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <iomanip>
#include <sstream>

#include <vector>

#include "unbiased.hh"
#include "sde.hh"
#include "discrete_geom.hh"
#include "math_helpers.hh"
#include "types.hh"
#include "helpers.hh"

using namespace std;

unbiased::unbiased(simulator &sim_, discrete_geom& p_, 
                   uint64_t n_, bool vars_calc_, bool sum_estimator_, 
                   bool coupled_, uint64_t seed_) 
: p{p_}, n{n_}, vars_calc{vars_calc_}, 
  seed{seed_}, sum_estimator{sum_estimator_}, coupled{coupled_} { /*{{{*/
  
  DEBUG("Creating unbiased");
  // Set dimension of functional
  d_f = sim_.get_d();
  
  // Make a copy of the simulator instance (for threads)
  sim = sim_.clone();
  // Initialise data for level_diff
  delta = new double[d_f];
  // Initialise storage for indices
  R = new unsigned int[n];
  
  // And set allocation and active level variables to zero
  m_alloc = 0; m = 0;
  
  // Draw (hopefully) a "real" random number
  //seed = std::random_device()();
  // To seed the generator
  generator = PRNG(seed);
}
/*}}}*/

unbiased::~unbiased() { /*{{{*/
  delete [] R;
  delete [] delta;
  delete sim;
  deallocate_means();
  DEBUG("Deleted unbiased.")
}
/*}}}*/

vector<uint64_t> unbiased::get_nlev() { /*{{{*/
  vector<uint64_t> n_lev_(m);
  for (int i=0; i<m; i++) n_lev_[i] = n_lev[i];
  return n_lev_;
}
/*}}}*/

vector<vector<double>> unbiased::get_means() { /*{{{*/
  vector<vector<double>> means_(m);
  for (int i=0; i<m; i++) {
    for (int j=0; j<d_f; j++)
    means_[i].push_back(means[i*d_f+j]);
  }
  return means_;
}
/*}}}*/

vector<vector<double>> unbiased::get_vars() { /*{{{*/
  if (!vars_calc) {
    return vector<vector<double>>(1);
  }
  vector<vector<double>> vars_(m);
  for (int i=0; i<m; i++) {
    for (int j=0; j<d_f; j++)
    vars_[i].push_back(vars[i*d_f+j]);
  }
  return vars_;
}
/*}}}*/

void unbiased::deallocate_means() { /*{{{*/
  if (m_alloc > 0) {
    delete [] means;
    delete [] n_lev;
    if (vars_calc) {
      delete [] vars;
    }
    m_alloc = 0;
  }
}
/*}}}*/

void unbiased::allocate_means(int m_) { /*{{{*/
  m = m_;
  if (m > m_alloc) {
    deallocate_means();
    try {
      means = new double[d_f*m]();   // Make sure zero
      n_lev = new uint64_t[m](); // Make sure zero
      if (vars_calc) vars = new double[d_f*m]();
    } catch(int e) {
      error("Failed to allocate space for means; exiting.");
    }
    m_alloc = m;
  } else {
    for (int i=0; i<d_f*m; i++) {
      means[i] = 0.0;
      if (vars_calc) vars[i] = 0.0;
    }
    for (int i=0; i<m; i++) n_lev[i] = 0;
  }
}
/*}}}*/

vector<double> unbiased::get_est() { /*{{{*/
  vector<double> y(d_f);
  double corr;
  int i,j;
  for (j=0; j<d_f; j++) y[j] = 0.0;
  for (i=0; i<m; i++) {
    // Nothing to do
    if (n_lev[i] == 0) continue;
    // Calculate "correction factor" to make unbiased
    if (sum_estimator) {
      corr = double(n_lev[i])/(double(n)*p.pmf_tilde(i+1));
    } else { // single-term
      corr = double(n_lev[i])/(double(n)*p.pmf(i+1));
    }
    for (j=0; j<d_f; j++) {
      // Update estimate
      y[j] += corr*means[i*d_f + j];
    }
  }
  return y;
}
/*}}}*/

vector<double> unbiased::get_est_var() { /*{{{*/
  double corr_var;
  if (!vars_calc) {
    return vector<double>(0);
  }
  vector<double> var(d_f);
  for (int i=0; i<m; i++) {
    if (n_lev[i] == 0) continue;
    if (sum_estimator) {
      corr_var = 1.0/(double(n)*p.pmf_tilde(i+1));
    } else { // single-term
      corr_var = 1.0/(double(n)*p.pmf(i+1));
    }
    for (int j=0; j<d_f; j++) {
      var[j] += corr_var*vars[i*d_f+j];
    }
  }
  return var;
}
/*}}}*/

void unbiased::update_means(unsigned int i) { /*{{{*/
  double d_;
  uint64_t cur_nlev = ++n_lev[i];
  double one_over_cur_nlev = 1.0/double(cur_nlev);
  
  if (vars_calc && cur_nlev > 1) {
    for (int j=0; j<d_f; j++) {
      d_ = delta[j] - means[i*d_f + j];
      vars[i*d_f + j] *= double(cur_nlev-2)/double(cur_nlev-1);
      vars[i*d_f + j] += one_over_cur_nlev*d_*d_;
    }
  }
  for (int j=0; j<d_f; j++) {
    means[i*d_f + j] *= double(cur_nlev-1)*one_over_cur_nlev;
    means[i*d_f + j] += one_over_cur_nlev*delta[j];
  }
}
/*}}}*/

string unbiased::stats_string() { /*{{{*/
  ostringstream out;

  int i,j;
  uint64_t n_ = 0;
  double std;
  
  vector<double> est = get_est();
  vector<double> est_var = get_est_var();
  
  out << scientific << setprecision(6);
  for (i=0; i<m; i++) {
    // Nothing to do
    if (n_lev[i] == 0) continue;
    n_ += n_lev[i];
    // Calculate "correction factor" to make unbiased
    for (j=0; j<d_f; j++) {
      // Update estimate
      out << left << "L" << setw(2) << i+1;
      if (d_f>1) out << "i" << setw(1) << j;
      out << " n=" << setw(8) << n_lev[i];
      out << "mean="; out << setw(14) << means[i*d_f + j];
      if (vars_calc) {
        std = sqrt(vars[i*d_f+j]);
        out << "tol=" << setw(14) << std/sqrt(double(n_lev[i]));
        out << "std="; out << setw(14) << std;
      }
      out << endl;
    }
  }
  out << setfill('-') << setw(72) << "-" << endl;
  out << setfill(' ');
  for (j=0; j<d_f; j++) {
    out << "TOT";
    if (d_f>1) out << "i" << setw(1) << j;
    out << " n=" << setw(8) << n_;
    out << "mean="; out << setw(14) << est[j];
    if (vars_calc) {
      out << ".95CI(" << est[j]-1.96*sqrt(est_var[j])
           << ", " << est[j]+1.96*sqrt(est_var[j]) << ")";
    }
    out << endl;
  }
  return(out.str());
}
/*}}}*/

void unbiased::estimate() { /*{{{*/
  // Draw level determining variables
  p.simulate_R(R, n, generator);
  int m = max(R, n);
  
  // Initialise variables and allocate (more) space if necessary
  DEBUG("unbiased::estimate() allocating space for means")
  allocate_means(m);
  
  
  if (sum_estimator) {
    for (uint64_t i=0; i<n; i++) {
      int cur_lev = R[i] - 1;
      if (coupled) (sim->simulate_bm)(cur_lev, generator);
      for (int j=0; j<=cur_lev; j++) {
        if (!coupled) (sim->simulate_bm)(j, generator);
        DEBUG("Calculating level diff " << j)
        (sim->level_diff)(delta, j, j-1);
        update_means(j);
      }
    }
  } else { // single-term
    for (uint64_t i=0; i<n; i++) {
      int cur_lev = R[i] - 1;
      // Simulate Brownian motion with sufficient discretisation
      (sim->simulate_bm)(cur_lev, generator);
      (sim->level_diff)(delta, cur_lev, cur_lev-1);
      update_means(cur_lev);
    }
  }
}
/*}}}*/

uint64_t unbiased::get_cost() { /*{{{*/
  unsigned int c = 0;
  for (int i=0; i<m; i++) c += sim->cost(i,i-1) * n_lev[i];
  return c;
}
/*}}}*/
