/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <fstream>
#include <string>
#include <vector> 
#include <random>
#include <thread>
#include <mutex>
#include <chrono>
#include <condition_variable>

extern "C" {
// For dynamic libraries
#include <dlfcn.h>
}

#include "discrete_geom.hh"
#include "sde_euler.hh"
#include "sde_milstein.hh"
#include "sde_antithetic.hh"
#include "unbiased.hh"
#include "types.hh"
#include "lua_env.hh"
#include "unbiased_io.hh"
#include "optimise.hh"
#include "counter.hh"

using namespace std;

// Time interval (msec) in which to flush output / print verbose output
const unsigned int write_int = 5e3; 

/**
 * Print help and exit
 */
void print_help(const char *progname) { /*{{{*/
  string pn(progname);
  cout << "Usage: " << pn << " model.config [-e 'inline config'] [-c out.config]" << endl;
  exit(0);
} /*}}}*/

/**
 * Read library file (or link relevant functions)
 */
void read_library(param& par) { /*{{{*/
  void* handle;
  try {
    handle = dlopen(par.model_lib.c_str(), RTLD_LAZY);
  } catch (int e) {
    error("Cannot upen model file: " + par.model_lib + ": " + dlerror());
  }
  try {
    par.mu = (param_func)dlsym(handle, "mu");
    par.sigma = (param_func)dlsym(handle, "sigma");
    if (par.sde_scheme == "milstein" || par.sde_scheme == "antithetic") {
      par.dsigma = (param_func)dlsym(handle, "dsigma");
    }
    par.func = (functional)dlsym(handle, "f");
  } catch(int e) {
    error("Cannot find symbols: " + par.model_lib + ": " + dlerror());
  }
}
/*}}}*/

/**
 * Read simple parameter variables.
 */
void read_simple(lua_env& L, param& par) { /*{{{*/
  
  DEBUG("Reading strings.")
  par.model_lib = L.get_string("lib", string());
  par.sde_scheme = L.get_string("sde_scheme", "euler");
  par.sampling_scheme = L.get_string("sampling_scheme", "strat");
  par.out_file = L.get_string("out_file", "-");
  par.stats_file = L.get_string("stats_file", string());

  // If stdout, default instead to CSV
  bool default_binary = true;
  if (par.out_file == "-") default_binary = false;
  
  DEBUG("Reading numeric values.")
  par.coupled = L.get_number<bool>("coupled", false);
  par.binary = L.get_number<bool>("binary", default_binary);
  par.d_f = L.get_number<int>("d_f", 1);
  par.T = L.get_number<double>("T", 1.0);
  par.n_samples = L.get_number<uint64_t>("n", 1000);
  
  par.tol = L.get_vector<double>("tol", vector<double>(0));
  uint64_t batch_default = 10;
  if (par.tol.size() > 0) batch_default = 0xFFFFFFFFFFFFFFFF;
  par.batch = L.get_number<uint64_t>("batch", batch_default);
  
  par.tail_prob = L.get_number<double>("tail_prob", 1.0);
  par.gamma = L.get_number<double>("gamma", 1.25);
  par.n_threads 
  = L.get_number<unsigned int>("n_threads", 
                               thread::hardware_concurrency());
  par.calc_vars = L.get_number<bool>("calc_vars", true);
  
  // By default, use single-term estimator, unless coupled
  bool default_sum = false;
  if (par.coupled == true) default_sum = true;
  par.sum = L.get_number<bool>("sum", default_sum);
  // Coupled single-term is single-term, so do not issue warnings
  
  par.summary = L.get_number<bool>("summary", false);
  par.optimise = L.get_number<bool>("optimise", false);
  par.base = L.get_number<unsigned int>("base", 2);
  par.max_level = L.get_number<int>("max_level", -1);
  
  if (par.optimise && par.sum) {
    error("Optimisation only available with single-term estimator!");
  }
  
  DEBUG("Reading vectors.")
  par.seed 
    = L.get_vector<uint64_t>("seed", vector<uint64_t>());
  par.x0 = L.get_vector<double>("x0", vector<double>(1, 0.0));
  par.p = L.get_vector<double>("p", vector<double>());
  par.levels = L.get_vector<int>("levels", vector<int>());
}
/*}}}*/

template<typename t> 
void write_numeric_vector(vector<t> &x, ostream& os) { /*{{{*/
  os << "{";
  for (unsigned int i=0; i<x.size(); i++) {
    if (i>=1) os << ",";
    os << x[i];
  }
  os << "}";
}
/*}}}*/

/**
 * Write configuration file.
 */
void write_config(param &par, string& fname) { /*{{{*/
  ofstream of(fname.c_str());
  if (!of.good()) error("Cannot write config file '" + fname + "'");
  of << "lib=\"" << par.model_lib << "\"" << endl;
  of << "d_f=" << par.d_f << endl;
  of << "x0="; write_numeric_vector(par.x0, of); of << endl;
  of << "T=" << par.T << endl;
  of << "sde_scheme=\"" << par.sde_scheme << "\"" << endl << endl;

  of << "levels="; write_numeric_vector(par.levels, of); of << endl;
  of << "p="; write_numeric_vector(par.p, of); of << endl;
  of << "tail_prob=" << par.tail_prob << endl;
  of << "gamma=" << par.gamma << endl;
  of << "sampling_scheme=\"" << par.sampling_scheme << "\"" << endl;
  of << "coupled=" << par.coupled << endl;
  of << "sum=" << par.sum << endl;
  of << "base=" << par.base << endl;
  of << "tol="; write_numeric_vector(par.tol, of); of << endl;
  of << "batch=" << par.batch << endl;
  of << "n=" << par.n_samples << endl << endl;
  
  of << "out_file=\"" << par.out_file << "\"" << endl;
  of << "stats_file=\"" << par.stats_file << "\"" << endl;
  of << "binary=" << par.binary << endl;
  of << "n_threads=" << par.n_threads << endl;
  of << "max_level=" << par.max_level << endl;
  of << "calc_vars=" << par.calc_vars << endl;
  of << "summary=" << par.summary << endl;
  of << "optimise=" << par.optimise << endl;
  of << "seed="; write_numeric_vector(par.seed, of); of << endl;
  of.close();
}
/*}}}*/

/**
 * Start simulator engine.
 */
simulator* new_engine(param& par) { /*{{{*/
  // Target func struct
  target_func f;
  f.func = par.func;
  f.d = par.d_f;
  
  simulator* engine = 0;
  if (par.sde_scheme == "euler") {
    engine = new sde_euler(par.mu, par.sigma, f, par.x0, par.T, 
                           par.base, par.levels, par.max_level);
  } else if (par.sde_scheme == "milstein") {
    engine = new sde_milstein(par.mu, par.sigma, par.dsigma, f, 
                              par.x0, par.T, par.base, par.levels, 
                              par.max_level);
  } else if (par.sde_scheme == "antithetic") {
    engine = new sde_antithetic(par.mu, par.sigma, par.dsigma, f, 
                                par.x0, par.T, par.base, par.levels, 
                                par.max_level);
  } else error("Unknown 'sde_scheme' " + par.sde_scheme);
  return(engine);
}
/*}}}*/

/**
 * The main worker thread(s).
 */
void worker(param& par, 
            unsigned int seed, unbiased_io<data_item>& out,
           unbiased_io<stats_item>& stats_out,
           counter& count) { /*{{{*/

  DEBUG("Thread " << this_thread::get_id() << ": Creating discrete_geom")
  discrete_geom dg(par.gamma, par.tail_prob, par.p, par.sampling_scheme);

  DEBUG("Thread " << this_thread::get_id() << ": Creating sde_engine.")
  simulator* engine = new_engine(par);
  
  DEBUG("Thread " << this_thread::get_id() << ": Creating unbiased sampler.")
  unbiased sampler(*engine, dg, par.n_samples, par.calc_vars, 
                   par.sum, par.coupled, seed);
  
  data_item data;
  stats_item stats;
  while(true) {
    if (!count.ask()) break;
    DEBUG("Thread " << this_thread::get_id() << " start cycle")
    sampler.estimate();
    if (out.active || par.summary || par.verbose) {
      data.m = sampler.get_est();
      data.v = sampler.get_est_var();
      data.c = sampler.get_cost();
      out.add(data);
    }
    if (stats_out.active || par.summary || par.optimise) {
      stats.m = sampler.get_means();
      stats.v = sampler.get_vars();
      stats.n = sampler.get_nlev();
      stats_out.add(stats);
    }
    //DEBUG("Thread " << this_thread::get_id() << ":");
    //DEBUG(sampler.stats_string())
  }
  delete engine;
  DEBUG("Thread " << this_thread::get_id() << " ends.")
}
/*}}}*/

int main(int argc, char **argv) {
  int i;
  param par;
  par.verbose = false; // Whether to print output during simulation
  string out_config_file;
  
  par.dsigma = 0;
  par.func = 0;
  
  lua_env L;
  
  for (i=1; i<argc; i++) {
    if (string(argv[i]) == "-v") {
      par.verbose = true;
      continue;
    } else if (string(argv[i]) == "-c") {
      if (++i>=argc) {
        // There must be a file as well
        print_help(argv[0]);
      } else {
        out_config_file = string(argv[i]);
      }
      continue;
    } else if (string(argv[i]) == "-e") {
      if (++i>=argc) {
        // There must be a file as well
        print_help(argv[0]);
      } else {
        par.inline_config = string(argv[i]);
      }
    } else {
      par.config_file=string(argv[i]);
    }
  }
  
  // If no config at all:
  if (par.config_file.empty() && par.inline_config.empty()) {
    print_help(argv[0]);
  }
  if (!par.config_file.empty()) 
    L.do_file(par.config_file);
  if (!par.inline_config.empty()) 
    L.do_eval(par.inline_config);
  
  // Simple variables
  read_simple(L, par);

  // Check if at least readable
  ifstream in_file(par.model_lib.c_str());
  if (!in_file.good()) {
    error("Cannot open lib file: " + par.model_lib);
  }
  in_file.close();

  DEBUG("Opening library file.")
  read_library(par);
  
  // Sanity checks
  if (par.p.size()==0 && par.tail_prob <= 0.0) 
    error("'tail_prob' cannot be zero if 'p' is not set."); 
  // ...
  if (par.n_threads<1)
    error("Number of threads must be positive.");
  if (par.sde_scheme == "milstein" && par.x0.size()>1)
    error("Milstein scheme only allows a one dimensional model.");
  if (par.tol.size() > 0 && par.tol.size() != par.d_f)
    error("Parameter 'tol' must be a vector of length 'd_f'.");
  
  // All threads seeded  
  if (par.seed.size()==0) {
    for (unsigned int j=0; j<par.n_threads; j++) {
      par.seed.push_back(random_device()());
    }
  } else if (par.seed.size() != par.n_threads) 
    error("'seed' must be a table with seeds for all threads.");
  
  if (par.sampling_scheme == "ml") {
    discrete_geom dg(par.gamma, par.tail_prob, par.p, 
                        par.sampling_scheme);
    uint64_t n_i, n_ = 0;
    vector<double> p_ml;
    if (par.sum) {
      int m = dg.find_m_tilde(1.0/double(par.n_samples));
      p_ml = vector<double>(m);
      for (i=1; i<=m; i++) { 
        n_i = floor(dg.pmf_tilde(i)*double(par.n_samples));
        p_ml[i-1] = n_i;
      }
      // Now p_ml contains p_tilde; work backwards to find p
      for (i=1; i<=m-1; i++) {
        p_ml[i-1] = p_ml[i-1] - p_ml[i];
        n_ += p_ml[i-1];
      }
      n_ += p_ml[m-1];
    } else { // single-term
      int m = dg.find_m(1.0/double(par.n_samples));
      p_ml = vector<double>(m);
      for (i=1; i<=m; i++) { 
        n_i = floor(dg.pmf(i)*double(par.n_samples));
        p_ml[i-1] = n_i;
        n_ += n_i;
      }
    }
    par.n_samples = n_;
    par.p = p_ml;
    par.tail_prob = 0.0;
  }

  DEBUG("Opening output file" << par.out_file)
  unbiased_io<data_item> writer(par.out_file, par.binary, 
                                par.summary || par.verbose);
  DEBUG("Opening stats output file." << par.stats_file)
  unbiased_io<stats_item> stats_writer(par.stats_file, par.binary, 
                                       par.optimise || par.summary);
  
  // Prepare for threading
  vector<thread> th(par.n_threads);
  
  // Set the number of batches to be eaten by workers
  counter count(par.batch, par.tol);
  
  DEBUG("Starting threads.")
  // Create instances of all samplers, seeded with the given seeds
  for (unsigned int j=0; j<par.n_threads; j++) {
    th[j] = thread(&worker, std::ref(par), par.seed[j], 
                   std::ref(writer), std::ref(stats_writer), 
                   std::ref(count));
  }
  
  DEBUG("Flushing output every " << double(write_int)/1e3 << " seconds.")
  mutex mtx;
  unique_lock<mutex> lock(mtx);
  while (count.to_do) {
    count.signal_end.wait_for(lock, chrono::milliseconds(write_int));
    if (par.verbose) cout << "\r" << data_item::status();
    DEBUG("Flushing")
    writer.flush();
    stats_writer.flush();
  }
  if (par.verbose) cout << endl;
  DEBUG("Waiting for threads to finish.")
  for (unsigned int j=0; j<par.n_threads; j++) {
    th[j].join();
  }
  
  if (par.optimise) {    
    DEBUG("Setting optimal distribution parametres 'p' and 'tail_prob'.")
    simulator* engine = new_engine(par);
    optimise::set_optimal(par.p, par.tail_prob, engine);
  }
  
  DEBUG("Writing config file.")
  if (!out_config_file.empty()) write_config(par, out_config_file);
  
  if (par.summary) {
    writer.flush();
    stats_writer.flush();
    cout << data_item::summary();
    cout << stats_item::summary();
  }
  DEBUG("All done, finishing")
}
