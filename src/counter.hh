/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _COUNTER_HH_
#define _COUNTER_HH_

#include <mutex>
#include <condition_variable>

#include "unbiased_io.hh"

using namespace std;

// Class that controls the total amount of work.
// 
class counter {
 private:
   // Counter how many jobs need to be done
   uint64_t value;
   
   // Just to protect when counter value changed.
   mutex guard;
   
   // Whether tolerance is checked
   bool check_tol;
   
   // Individual tolerance level for each component of f
   vector<double> tol;
   
 public:
   // Condition variable to signal exit as soon as done
   condition_variable signal_end;
   
   // Set the initial value
   counter(uint64_t value_, const vector<double>& tol_);

   // Ask if more samples needed
   bool ask();
   
   // Tell to end
   void end();
   
   // Check this if there are still things to do
   bool to_do;
};

#endif
