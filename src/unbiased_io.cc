/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "unbiased_io.hh"
#include "types.hh"

// Number of samples in the cumulative estimate
mutex data_item::guard;
uint64_t data_item::n_records = 0;
vector<double> data_item::mean_est(0);
vector<double> data_item::mean_var(0);
vector<double> data_item::emp_var(0);

void data_item::add_hook() { /*{{{*/
  guard.lock();
  n_records++;
  if (n_records == 1) {
    // First record, initialise estimators
    mean_est = m;
    mean_var = v;
    emp_var = vector<double>(m.size());
  } else {
    double one_over_nrec = 1.0/double(n_records);
    for (unsigned int i=0; i<m.size(); i++) {
      if (n_records >= 2) {
        double d = m[i]-mean_est[i];
        emp_var[i] = 
        double(n_records-2)/double(n_records-1)*emp_var[i]
        + d*d*one_over_nrec;
      }
      mean_est[i] = 
      double(n_records-1)/double(n_records)*mean_est[i] + m[i]*one_over_nrec;
      mean_var[i] = 
      double(n_records-1)/double(n_records)*mean_var[i] + v[i]*one_over_nrec;
    }
  }
  guard.unlock();
}
/*}}}*/

void data_item::finalise_hook() {}

string data_item::status() { /*{{{*/
  ostringstream out;
  guard.lock();
  unsigned int i, tol = 1; // Default
  if (n_records > 1) {
    for (i=0; i<emp_var.size(); i++) {
      double rel_err = -log10(sqrt(emp_var[i])/(abs(mean_est[i])*double(n_records-1)));
      if (rel_err > tol) tol = ceil(rel_err);
    }
  }
  out << scientific << setprecision(tol) << left;
  for (i=0; i<mean_est.size(); i++) {
    if (i>0) out << ", ";
    out << setw(6+tol) << mean_est[i];
    if (n_records > 1) {
      out << " (" << setw(6+tol) << sqrt(emp_var[i]/double(n_records-1)) << ")";
    }
  }
  guard.unlock();
  return out.str();
} /*}}}*/

string data_item::summary() { /*{{{*/
  ostringstream out; 
  out << scientific << setprecision(6) << left;
  out << "Final estimate:" << endl;
  out << "---------------" << endl;
  unsigned int i;
  if (n_records == 0) {  
    out << "Nothing to report." << endl;
  } else {
    out << setw(16) << "Mean: ";
    for (i=0; i<mean_est.size(); i++) {
      if (i>0) out << ",";
      out << mean_est[i];
    }
    out << endl;
    if (n_records >= 2) {
      out << setw(15) << "95\% CI: ";
      for (i=0; i<emp_var.size(); i++) {
        double ci = 1.96*sqrt(emp_var[i]/double(n_records));
        if (i>0) out << ",";
        out << "(" << mean_est[i] - ci << "," << mean_est[i] + ci << ")";
      }
      out << endl;
    }
    /*out << setw(15) << "Asympt. 95\%CI: ";
    for (i=0; i<mean_var.size(); i++) {
      double ci = 1.96*sqrt(mean_var[i]/double(n_records));
      if (i>0) out << ",";
      out << "(" << mean_est[i] - ci << "," << mean_est[i] + ci << ")";
    }
    out << endl;*/
  }
  out << endl;
  return out.str();
} /*}}}*/

void data_item::write_header(ostream& os) { /*{{{*/
  unsigned int i;
  for (i=0; i<m.size(); i++) {
    if (i>=1) os << ",";
    os << "m_" << i+1;
  }
  for (i=0; i<v.size(); i++) 
    os << ",v_" << i+1;
  os << ",c" << endl;
}
/*}}}*/

void data_item::write_binary_record(ostream& os) { /*{{{*/
  double c_ = (double)c;
  os.write(reinterpret_cast<const char*>(m.data()), 
            sizeof(double)*m.size());
  os.write(reinterpret_cast<const char*>(v.data()), 
            sizeof(double)*v.size());
  os.write(reinterpret_cast<const char*>(&c_), 
             sizeof(double));
}
/*}}}*/

void data_item::write_ascii_record(ostream& os) { /*{{{*/
  unsigned int i;
  for (i=0; i<m.size(); i++)  {
    if (i>=1) os << ",";
    os << m[i];
  }
  for (i=0; i<v.size(); i++) 
    os << "," << v[i];
  os << "," << c << endl;
}
/*}}}*/

bool data_item::tol_ok(vector<double>& tol) { /*{{{*/
  if (n_records <= 1) return false;
  bool ret = true;
  guard.lock();
  for (unsigned int i=0; i<tol.size(); i++)  {
    if (emp_var[i]/double(n_records) > tol[i]) ret = false;
  }
  guard.unlock();
  return ret;
}
/*}}}*/


mutex stats_item::guard;
uint64_t stats_item::n_records = 0;
vector<vector<double>> stats_item::var_est(0);
vector<vector<double>> stats_item::mean_est(0);
vector<uint64_t> stats_item::n_tot(0);
unsigned int stats_item::d = 0;

void stats_item::add_hook() { /*{{{*/
  guard.lock();
  n_records++;
  unsigned int i, j;
  if (n_records == 1) {
    n_tot = n;
    var_est = vector<vector<double>>(v.size());
    mean_est = vector<vector<double>>(m.size());
    for (i=0; i<v.size(); i++) {
      var_est[i] = v[i];
      mean_est[i] = m[i];
      d = v[i].size();
    }
  } else {
    unsigned int M = n.size();
    if (M>n_tot.size()) {
      // Allocate more space if necessary
      n_tot.resize(M, 0);
      var_est.resize(M, vector<double>(d, 0.0));
      mean_est.resize(M, vector<double>(d, 0.0));
    }
    for (i=0; i<M; i++) {
      uint64_t n_new = n_tot[i]+n[i];
      if (n_new == 0) continue;
      double one_over_n = 1.0/double(n_new);
      double ntot_over_n = double(n_tot[i])*one_over_n;
      double ni_over_n = double(n[i])*one_over_n;
      double one_over_n1 = 1.0/double(n_new-1);
      double ntot1_over_n1 = double(n_tot[i]-1)*one_over_n1;
      double ntot_over_n1 = double(n_tot[i])*one_over_n1;
      double ni1_over_n1 = double(n[i]-1)*one_over_n1;
      double ni_over_n1 = double(n[i])*one_over_n1;
      for (j=0; j<d; j++) {
        // Keep previous mean
        double m_old = mean_est[i][j];
        double m_add = m[i][j];
        // Update mean estimate
        double m_new = ntot_over_n*m_old + ni_over_n*m_add;
        mean_est[i][j] = m_new;
        if (n_new > 1) {
          double v_old = var_est[i][j];
          double v_add = v[i][j];
          var_est[i][j] = ntot1_over_n1*v_old + ntot_over_n1*(m_old - m_new)*(m_old  - m_new)
                        + ni1_over_n1*v_add + ni_over_n1*(m_add - m_new)*(m_add - m_new);
        }
      }
      n_tot[i] = n_new;
    }
  }
  guard.unlock();
}
/*}}}*/

void stats_item::write_header(ostream& os) { /*{{{*/
  os << "Each record is of the form: M,n_1,...,n_M,m_1_1,...,m_1_d,...,m_M_d,v_1_1,...,v_d_M" << endl;
}
/*}}}*/

void stats_item::write_binary_record(ostream& os) { /*{{{*/
  unsigned int i, M = n.size();
  os.write(reinterpret_cast<const char*>(&M), 
           sizeof(uint64_t));
  os.write(reinterpret_cast<const char*>(n.data()), 
             sizeof(uint64_t)*n.size());
  for (i=0; i<m.size(); i++) {
    os.write(reinterpret_cast<const char*>(m[i].data()), 
             sizeof(double)*m[i].size());
  }
  for (i=0; i<v.size(); i++) {
    os.write(reinterpret_cast<const char*>(v[i].data()), 
             sizeof(double)*v[i].size());
  }
}
/*}}}*/

void stats_item::write_ascii_record(ostream& os) { /*{{{*/
  unsigned int i, j, M = n.size();
  os << M;
  for (i=0; i<n.size(); i++) {
    os << "," << n[i];
  }
  for (i=0; i<m.size(); i++)  {
    for (j=0; j<m[i].size(); j++) {
      os << "," << m[i][j];
    }
  }
  for (i=0; i<v.size(); i++) {
    for (j=0; j<v[i].size(); j++) {
      os << "," << v[i][j];
    }
  }
  os << endl;
}
/*}}}*/

string stats_item::summary() { /*{{{*/
  ostringstream out;
  if (n_records > 0 ) {
    out << scientific << setprecision(6) << left;
    out << "Lev. Total num. ";
    out << setw(mean_est[0].size()*14) << "Mean";
    out << setw(var_est[0].size()*14) << "Sd." << endl;
    out.fill('-');
    int width = 16+(mean_est[0].size()+var_est[0].size())*14;
    out << setw(width) << "" << endl;
    out.fill(' ');
    for (unsigned int i=0; i<var_est.size(); i++) {
      out << setw(5) << i+1;
      out << setw(11) << n_tot[i];
      for (unsigned int j=0; j<d; j++) 
      out << setw(14) << mean_est[i][j];
      for (unsigned int j=0; j<d; j++) 
      out << setw(14) << sqrt(var_est[i][j]);
      out << endl;
    }
  }
  return out.str();
}
/*}}}*/

void stats_item::finalise_hook() {}

void stats_item::get_est(vector<uint64_t>& n_tot_, vector<vector<double>>& var_est_) { /*{{{*/
  n_tot_ = n_tot;
  unsigned int M = var_est.size();
  var_est_ = vector<vector<double>>(M);
  for (unsigned int i=0; i<M; i++) var_est_[i] = var_est[i];
}
/*}}}*/

