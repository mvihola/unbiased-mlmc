/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <random>
#include <cstring>
#include <cassert>

#include "sde_euler.hh"
#include "math_helpers.hh"
#include "types.hh"

using namespace std;

sde_euler::sde_euler(param_func mu_, param_func sigma_, 
                     target_func f_, 
                     const vector<double> &x0_, double T_, int base_,
                    const vector<int> &active_levels_, 
                    int max_level_)
: sde(f_, x0_, T_, base_, active_levels_, max_level_) { /*{{{*/
  DEBUG("Creating sde_euler")
  // Just set these to what we need
  mu = mu_;
  sigma = sigma_;
  // Then allocate a bit of memory for temp stuff
  x = new double[d];
  x_ = new double[d];
  dB = new double[d];
  mu_x = new double[d*d];
  sigma_x = new double[d*d];
}
/*}}}*/

sde_euler::~sde_euler() { /*{{{*/
  delete[] x;
  delete[] x_;
  delete[] mu_x;
  delete[] sigma_x;
  delete[] dB;
  DEBUG("Deleted sde_euler")
}
/*}}}*/

void sde_euler::level(double *y, int n) { /*{{{*/
  int i,j;
  int nb = base_n(n_bm), ninc = base_n(n_bm-n);
  double* tmp;
  double t, T_over_nb = T/double(nb);
  double dt = double(ninc)*T_over_nb;
  assert(n<=n_bm);
  
  // Initialise state
  for (j=0; j<d; j++) x[j] = x0[j];
  
  // Main loop
  for (i=0; i<nb; i+=ninc) {
    // Current time
    t = double(i)*T_over_nb;

    // Calculate parameters
    mu(x, t, mu_x);
    sigma(x, t, sigma_x);

    // Calculate Brownian increment
    for (j=0; j<d; j++) {
      dB[j] = B[j+d*(i+ninc)] - B[j+d*i];
    }
    // Calculate parameters
    mu(x, t, mu_x);
    sigma(x, t, sigma_x);
    // Do the update: x_ = x + dt*mu_x
    vec_mpy_scalar_add(x, mu_x, dt, x_, d);
    // and x_ = x_ + sigma_x*dB
    mat_vec_mpy_add(sigma_x, dB, x_, d);
    // Swap pointers (yes, dodgy, but fast)
    tmp = x_; x_ = x; x = tmp;
  }
  f.func(x, y);
}
/*}}}*/

simulator* sde_euler::clone() { /*{{{*/
  return static_cast<simulator*>(new sde_euler(*this));
}
/*}}}*/
