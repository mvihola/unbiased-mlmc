/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _UNBIASED_HH_
#define _UNBIASED_HH_

#include <random>
#include <thread>
#include <mutex>

#include "types.hh"
#include "simulator.hh"
#include "discrete_geom.hh"

using namespace std;

/**
 * The unbiased class implements the unbiased sampling schemes
 * as explained in the paper. The discrete_geom class is passed
 * to the unbiased class.
 */
class unbiased {
 private:
   // The single difference vector
   double* delta;
   // Dimensions of state and functional
   int d_f;
   // Array of means of all levels
   double* means;
   // Array of variances of all levels
   double* vars;
   // Flag whether these calculated
   bool vars_calc;
   // Whether we use sum estimator instead of single-term
   bool sum_estimator;
   // whether we use coupled estimator
   bool coupled;
   // Number of samples per level
   uint64_t* n_lev;
   // Number of samples
   uint64_t n;
   // Number of active and allocated levels
   int m, m_alloc;
   // The random variables determining the levels
   unsigned int* R;
   // The distribution determining R
   discrete_geom p;
   // The SDE array
   simulator* sim;
   
   // PRNG seed
   uint64_t seed;
   
   // Pseudo-random number generator
   PRNG generator;
   
   void allocate_means(int);
   void deallocate_means();
   void update_means(unsigned int level);

 public:
   // Constructor, taking any SDE type, sampling scheme for levels,
   // target functional, number of samples and a flag whether variance should
   // be estimated
   unbiased(simulator&, discrete_geom&, 
            uint64_t n = 100, bool variance = false, 
            bool sum_estimator = false,
            bool coupled = false,
            uint64_t seed = random_device()());
   // Destructor
   ~unbiased();
   // Run an estimation cycle
   void estimate();
   
   // Get the estimate
   vector<double> get_est();
   // Get the estimate variance
   vector<double> get_est_var();
   
   // Get number of levels
   vector<uint64_t> get_nlev();
   // Get means
   vector<vector<double>> get_means();
   // Get variances
   vector<vector<double>> get_vars();
   // Get the cost of the current estimate
   uint64_t get_cost();
   
   // Print stats of the estimation
   string stats_string();
};


#endif
