/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <algorithm> // std::copy_n
#include <iostream>
#include <sstream>
#include <string>
#include <limits>
#include <cassert>

#include <cmath> // pow
#include "discrete_geom.hh"

#include "helpers.hh"

using namespace std;

discrete_geom::discrete_geom(double gamma_, double tail_prob_,  
                             const vector<double> &p_,
                             string scheme_)
  : gamma{gamma_}, tail_prob{tail_prob_}, p{p_} { /*{{{*/

  int i;
  double sum = 0;
  n = p.size();
  N_resid = 0;
  resid = 0;
  
  sampling_scheme = scheme_;
  if (scheme_ == "strat") {
    scheme = STRATIFIED;
  } else if (scheme_ == "iid") {
    scheme = IID;
  } else if (scheme_ == "resid") {
    scheme = RESIDUAL;
  } else if (scheme_ == "syst") {
    scheme = SYSTEMATIC;
  } else if (scheme_ == "ml") {
    scheme = MLMC;
  } else {
    cerr << "Warning: Unknown sampling scheme '" << scheme_ 
              << "'; using 'strat' instead." << endl;
    scheme = STRATIFIED;
    sampling_scheme = "strat";
  }
  
  // This is used to simulate the samples
  uniform_real_distribution<double> runif(0.0, 1.0);
  
  // Calculate sum of elements
  for (i=0; i<n; i++) sum += p[i];
    
  // ...and normalise to sum to unity
  for (i=0; i<n; i++) p[i] /= sum;
  
  // Calculate distribution function
  update_df();
  DEBUG("Created discrete_geom: " << to_string())
}
/*}}}*/


discrete_geom::~discrete_geom() {
  if (N_resid > 0) delete resid;
  DEBUG("Deleted discrete_geom." << to_string())
}

// Calculate distribution function from mass function.
void discrete_geom::update_df() { /*{{{*/
  if (n == 0) return;
  df = p;
  // Calculate the cumulative distribution function
  for (int i=1; i<n; i++) {
    df[i] = df[i-1] + p[i];
  }
  // Enforce last to be exactly 1.
  df[n-1] = 1.0;
}
/*}}}*/

// The distribution is cq^i for i>=0.
double inline geom_pmf(double gamma, int i) { /*{{{*/
  double q = pow(2, -gamma); 
  double c = (1-q);
  double val = c*pow(q, i);
  return val;
}
/*}}}*/

// The tail prob. of cq^i for i>=0.
double inline geom_pmf_tilde(double gamma, int i) { /*{{{*/
  double q = pow(2, -gamma); 
  double val = pow(q, i);
  return val;
}
/*}}}*/

/** Probability mass function at i */
double discrete_geom::pmf(int i) { /*{{{*/
  double val;
  if (i<=0) {
    // Just for completeness...
    val = 0.0;
  } else if (i<=n) {
    // The custom bit.
    val = (1.0-tail_prob)*p[i-1];
  } else {
    // The tail bit.
    val = tail_prob*geom_pmf(gamma, i-n-1);
  }
  return val;
} /*}}}*/

/** Tail probability at >= i */
double discrete_geom::pmf_tilde(int i) { /*{{{*/
  double v = 0.0;
  if (i==1) {
    v = 1.0;
  } else if (i<=n) { // 2 <= i <= n
    v = 1.0 - (1.0-tail_prob)*df[i-2];
  } else if (i>n) {
    v = tail_prob*geom_pmf_tilde(gamma, i-n-1);
  }
  return v;
}
/*}}}*/

// Just a helper for debugging
string discrete_geom::to_string() { /*{{{*/
  ostringstream out;
  out <<  "gamma: " << gamma << " tail_prob: " << tail_prob <<" p:";
  for (int i=0; i<n; i++) {
    out << " " << p[i];
  }
  out << " df: ";
  for (int i=0; i<n; i++) {
    out << " " << df[i];
  }
  return(out.str());
}
/*}}}*/

// Inverse distribution function, for RV generation
int discrete_geom::F_inv(double U) { /*{{{*/
  int i;
  if (U<tail_prob) {
    // The tail
    // Normalise so that values in (0,1):
    U /= tail_prob;
    // The usual geometric RV generation
    i = floor(-log2(U)/gamma);
    return(i+n+1);
  } else {
    // The custom p:
    // Normalise so that values in (0,1):
    U = (U-tail_prob)/(1.0-tail_prob);
    // The usual inverse DF method
    for (i=0; i<n; i++) {
      if (df[i] >= U) break;
    }
    return(i+1);
  }
}
/*}}}*/

// Find largest m such that (1-2^(-gamma))2^(-gamma m)>=val
int inline find_m_geom(double gamma, double val, double tail_prob) { /*{{{*/
  int m;
  if (tail_prob == 0 || val == 0) {
    return(-1);
  } else {
    double c = (1-pow(2, -gamma));
    // Use round; may result in one too big m but avoids numerical problems...
    //m = floor((log2(tail_prob)+log2(1/val)+log2(c))/gamma);
    m = round((log2(tail_prob)+log2(1/val)+log2(c))/gamma);
  }
  return m;
}
/*}}}*/

// Find largest m such that the tail prob >= val
int inline find_m_geom_tilde(double gamma, double val, double tail_prob) { /*{{{*/
  int m;
  if (tail_prob == 0 || val == 0) {
    return -1;
  } else {
    // Use round; may result in one too big m but avoids numerical problems...
    //m = floor((log2(tail_prob)+log2(1/val))/gamma);
    m = round((log2(tail_prob)+log2(1/val))/gamma);
  }
  return m;  
}
/*}}}*/

int discrete_geom::find_m(double val) { /*{{{*/
  int m, m_;
  m_ = find_m_geom(gamma, val, tail_prob);
  if (m_ >= 0) {
    // This means found in the tail:
    m = n + 1 + m_;
  } else {
    // Otherwise go through custom
    m = 0;
    for (int i=1; i<=n; i++) {
      if (pmf(i)*(1+n*numeric_limits<double>::epsilon())  >= val) m = i;
    }
  }
  return m;
}
/*}}}*/

int discrete_geom::find_m_tilde(double val) { /*{{{*/
  int m, m_;
  m_ = find_m_geom_tilde(gamma, val, tail_prob);
  if (m_ >= 0) {
    // This means found in the tail:
    m = n + 1 + m_;
  } else {
    // Otherwise go through custom
    m = 0;
    for (int i=1; i<=n; i++) {
      if (pmf_tilde(i)*(1+n*numeric_limits<double>::epsilon()) >= val) m = i;
    }
  }
  return m;  
}
/*}}}*/

void discrete_geom::simulate_R(unsigned int* R, uint64_t N, PRNG& g) { /*{{{*/
  uint64_t i;
  int j, m;
  uint64_t n_j;
  double U, U_;
  double one_over_N = 1.0/double(N);
  switch(scheme) {
  case IID:
    for (i=0; i<N; i++) {
      U = runif(g);
      R[i] = F_inv(U);
    }
    break;
  case STRATIFIED:
    for (i=0; i<N; i++) {
      U = one_over_N*(double(i) + runif(g));
      R[i] = F_inv(U);
    }
    break;
  case SYSTEMATIC:
    U_ = runif(g);
    for (i=0; i<N; i++) {
      U = one_over_N*(double(i) + U_);
      R[i] = F_inv(U);
    }
    break;
  case MLMC:
    i=0;
    m = find_m(one_over_N);
    for (j=1; j<=m; j++) {
      // This is numerically safer than floor...
      n_j = round(double(N)*pmf(j));
      DEBUG(double(N)*pmf(j) << " vs. " << n_j)
      for (uint64_t k=0; k<n_j && i<N; k++) R[i++] = j;
    }
    DEBUG(N << " vs. " << i)
    assert(N == i);
    break;
  case RESIDUAL:
    m = find_m(one_over_N);
    vector<double> r;
    // Whether residual needs to be calculated
    bool CALC_RESID = (N_resid != N);
    if (CALC_RESID) {
      r = vector<double>(m); // The residual stored here
      if (N_resid > 0) delete resid; // Deallocate if redone
      N_resid = N;
    }
    uint64_t i_ = 0, N_ = 0;
    double p_ = 0.0;
    for (j=1; j<=m; j++) {
      double p_j = pmf(j);
      uint64_t n_j = floor(double(N)*p_j);
      N_ += n_j;
      // Set what we got
      for (uint64_t k=0; k<n_j; k++) R[i_++] = j;
      // 
      if (CALC_RESID) {
        // Calculate residual
        p_ += p_j;
        r[j-1] = double(N)*p_j - double(n_j);
      }
    }
    if (CALC_RESID) {
      // Form residual distribution from which samples drawn
      double resid_tail_prob = (1.0-p_)*double(N)/double(N-N_);
      resid = new discrete_geom(gamma, resid_tail_prob, r, "iid");
    }
    resid -> simulate_R(&R[i_], N-N_, g);
  }
}
/*}}}*/

