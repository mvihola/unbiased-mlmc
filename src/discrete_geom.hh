/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _DISCRETE_GEOM_HH_
#define _DISCRETE_GEOM_HH_

#include <vector>
#include <iostream>
#include <string>

#include "types.hh"

using namespace std;

// This is purely internal
enum sampler_type {
  IID,
  STRATIFIED,
  RESIDUAL,
  SYSTEMATIC,
  MLMC
};

/**
 * Discrete distribution with geometric tail.
 * 
 * This is a class which implements a discrete distribution
 * on the positive (>=1) integers. The first n probabilities
 * are given in a vector, and then the probability of the tail
 * is specified separately.
 */
class discrete_geom {
 private:
   vector<double> p;  /** The first n probabilities */
   int n;             /** Length of p */
   double gamma;           /** The tail parameter */
   double tail_prob;       /** The tail probability */

   vector<double> df; /** The distribution function corresp. p */

   void update_df();       /** Calculate the distribution function */
   
   string sampling_scheme; /** The supplied literal form */
   sampler_type scheme;    /** For internal use */
   
   uniform_real_distribution<double> runif;

   // Inverse df, which can be used for random number generation
   int F_inv(double U);
      
   // Residual sampling distribution
   discrete_geom* resid;
   
   // To avoid repeating the construction...
   uint64_t N_resid;
   
 public:
   // Constructor for the generic case
   discrete_geom(double gamma = 1.25, double tail_prob = 1.0, 
                 const vector<double> &p = vector<double>(0), 
                 string scheme = string("strat"));
   // Destructor
   ~discrete_geom();
   
   // Copy constructor
   discrete_geom(const discrete_geom& other) 
   : discrete_geom(other.gamma, other.tail_prob, other.p, other.sampling_scheme) {};
   
   // Probability mass function at i
   double pmf(int i);

   // Probability mass function at i
   double pmf_tilde(int i);
   
   // A helper which formats the contents; just for debugging...
   string to_string();
   
   // A function which simulates n samples R[i] from the distribution
   // using PRNG g
   void simulate_R(unsigned int* R, uint64_t n, PRNG& g);

   // Find highest i which pmf exceeds val
   int find_m(double val);

   // Find highest i which pmf exceeds val
   int find_m_tilde(double val);
   
   // Simple getters
   string get_sampling_scheme() { return sampling_scheme; };
   vector<double> get_p() { return p; };
   double get_tail_prob() { return tail_prob; };
   
};

#endif
