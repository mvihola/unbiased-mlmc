/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _MATH_HELPERS_HH_
#define _MATH_HELPERS_HH_

// Indexing matrices and 3-dim arrays
#define IND2(I,J,D) (I + D*J)
#define IND3(I,J,L,D) (I + D*J + D*D*L)

// y = y + A*x
void inline mat_vec_mpy_add(const double* A, const double* x, 
                        double* y, int d) { /*{{{*/
  for (int i=0; i<d; i++) {
    for (int j=0; j<d; j++) 
      y[i] += A[IND2(i,j,d)] * x[j];
  }
}
/*}}}*/

// y = x + s*inc
void inline vec_mpy_scalar_add(const double* x, const double* inc, double s, 
                               double* y, int d) { /*{{{*/
  for (int i=0; i<d; i++) y[i] = x[i] + s*inc[i];
}
/*}}}*/

// Find max of a vector
template <typename ctype> 
ctype max(ctype* X, int n) { /*{{{*/
   if (n <= 0) return((ctype)0);
   ctype m = X[0];
   for (int j=1; j<n; j++) m = (X[j]>m) ? X[j] : m;
   return m;
} /*}}}*/

#endif
