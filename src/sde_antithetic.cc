/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <random>
#include <cstring>
#include <cassert>

#include "sde_antithetic.hh"
#include "math_helpers.hh"
#include "types.hh"
#include "helpers.hh"

using namespace std;

sde_antithetic::sde_antithetic(param_func mu_, param_func sigma_, 
                     param_func dsigma_, target_func f_, 
                     const vector<double> &x0_, double T_,
                     int base_, const vector<int> &active_levels_,
                     int max_level_)
: sde(f_, x0_, T_, base_, active_levels_, max_level_) { /*{{{*/
  DEBUG("Creating sde_antithetic")
  // Just set these to what we need
  mu = mu_;
  sigma = sigma_;
  dsigma = dsigma_;
  // Then allocate a bit of memory for temp stuff
  try {
    x = new double[d];
    x_ = new double[d];
    dB = new double[d];
    dBdBT = new double[d*d];
    mu_x = new double[d*d];
    sigma_x = new double[d*d];
    dsigma_x = new double[d*d*d];
  } catch(int e) {
    error("sde_antithetic::sde_antithetic Failed to allocate memory.");
  }
  DEBUG("Created sde_antithetic with dim " << d)
}
/*}}}*/

sde_antithetic::~sde_antithetic() { /*{{{*/
  delete[] x;
  delete[] x_;
  delete[] dB;
  delete[] dBdBT;
  delete[] mu_x;
  delete[] sigma_x;
  delete[] dsigma_x;
  DEBUG("Deleted sde_antithetic")
}
/*}}}*/

// y = y + dsigma_x(*)(dB dB^T + I dt)
void sde_antithetic::milstein_inc(double dt, double* y) { /*{{{*/
  int i,j,k,l;
  // Calculate dB*dB^T
  for (j=0;j<d; j++) {
    for (k=0; k<d; k++) {
      dBdBT[IND2(j,k,d)] = dB[j]*dB[k];
    }
  }
  for (i=0; i<d; i++) {
    double inc = 0.0;
    for (j=0; j<d; j++) {
      for (k=0; k<d; k++) {
        // Calculate h_{ijk}
        double h = 0.0;
        for (l=0; l<d; l++) {
          h += .5*sigma_x[IND2(l,k,d)]*dsigma_x[IND3(i,j,l,d)];
        }
        if (j==k) {
          inc += h*(dBdBT[IND2(j,k,d)] - dt);
        } else {
          inc += h*dBdBT[IND2(j,k,d)];
        }
      }
    }
    y[i] = y[i] + inc;
  }
} /*}}}*/

inline void sde_antithetic::level(double* y, int n) { /*{{{*/
  milstein_level(y, n, 0);
}
/*}}}*/

void sde_antithetic::milstein_level(double *y, int n, int swap) { /*{{{*/
  int64_t i, j;
  // BM index increment for BM level n
  int64_t ninc = base_n(n_bm-n);
  int64_t nb = base_n(n);
  int64_t swap_inc = base_n(swap);
  
  int64_t start_ind, offset;
  int64_t ind1, ind2;
  double* tmp;
  double t, T_over_nbm = T/double(base_n(n_bm));
  double dt = double(ninc)*T_over_nbm;
  assert(n<=n_bm);
  
  // Initialise state
  for (j=0; j<d; j++) x[j] = x0[j];
  
  // Main loop
  for (i=0; i<nb; i++) {
    // Current time
    t = double(i*ninc)*T_over_nbm;

    DEBUG("sde_antithetic::milstein_level Calculaing Brownian increment t=" << t)
    // Calculate Brownian increment
    if (swap > 0) {
      // Offset from last coarse step point
      offset = i % swap_inc;
      // Last coarse step point
      start_ind = i - offset;
      // And we want time-reverse in between...
      j = start_ind + (swap_inc - offset);
      // So this is what we want
      ind1 = j;
      ind2 = (j-1);
    } else {
      // Otherwise, just linear indexing
      ind1 = (i+1); 
      ind2 = i;
    }
    for (j=0; j<d; j++) {
      dB[j] = B[d*ninc*ind1+j] - B[d*ninc*ind2+j];
    }
    
    // Calculate parameters
    DEBUG("sde_antithetic::milstein_level Evaluating model parameters t=" << t)
    mu(x, t, mu_x);
    sigma(x, t, sigma_x);
    dsigma(x, t, dsigma_x);

    // Do the update: x_ = x + dt*mu_x
    vec_mpy_scalar_add(x, mu_x, dt, x_, d);
    
    // and x_ = x_ + sigma_x*dB
    mat_vec_mpy_add(sigma_x, dB, x_, d);
    
    // x_ = x_ + dsigma_x(*)(dB*dB^T - I dt)
    milstein_inc(dt, x_);
    
    // Swap pointers (yes, dodgy, but fast)
    tmp = x_; x_ = x; x = tmp;
  }
  f.func(x, y);
}
/*}}}*/

void sde_antithetic::level_diff(double* y, int n_, int m_) { /*{{{*/
  int n = determine_level(n_);
  int m = determine_level(m_);
  unsigned int i;

  // If same level (in practice, beyond max_level), return zero
  if (n == m) {
    for (i=0; i<f.d; i++) y[i] = 0;
    return;
  }

  // Ensure that n>m>=-1
  assert((n>m) && (m>=-1));
    
  // Fine level
  DEBUG("sde_antithetic::level_diff Fine level n = " << n)
  milstein_level(y, n, 0);
  
  if (m>=0) {
    // ...and its antithetic pair
    DEBUG("sde_antithetic::level_diff Antithetic pair m = " << m)
    milstein_level(y_, n, n-m);
    for (i=0; i<f.d; i++) y[i] = .5*(y[i]+y_[i]);
    
    // Subtract the coarse level (which equals zero if m==-1)
    DEBUG("sde_antithetic::level_diff Coarse level")
    milstein_level(y_, m, 0);
    for (i=0; i<f.d; i++) y[i] -= y_[i];
  }
  DEBUG("sde_antithetic::level_diff Level complete")
}
/*}}}*/

simulator* sde_antithetic::clone() { /*{{{*/
  return static_cast<simulator*>(new sde_antithetic(*this));
}
/*}}}*/

uint64_t sde_antithetic::cost(int n_, int m_) { /*{{{*/
  int n = determine_level(n_);
  int m = determine_level(m_);  
  int c = 0;  
  if (n != m) {
    if (m<0) {
      c = base_n(n);
    } else {
      // Account for twice the cost of finer level
      c = (2*base_n(n)+base_n(m));
    }
  }
  return c;
}
/*}}}*/

