/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "counter.hh"

counter::counter(uint64_t value_, const vector<double>& tol_) 
: value{value_}, tol{tol_} {
  DEBUG("Creating counter class")
  if (tol.size() > 0) {
    check_tol = true;
    for (unsigned int i=0; i<tol.size(); i++) 
      tol[i] = tol[i]*tol[i];
  } else {
    check_tol = false;
  }
  to_do = true;
};

bool counter::ask() {
  // First just check this...
  if (!to_do) {
    return false;
  }
  
  // Otherwise check counter.
  guard.lock();
  if (value == 0) {
    to_do = false;
  } else {
    value--;
  }
  guard.unlock();
  
  if (!to_do) {
    signal_end.notify_all();
    return false;
  }
  
  bool ret = true;
  if (check_tol) {
    if (data_item::tol_ok(tol)) {
      to_do = false;
      ret = false;
    }
  }
  if (!to_do) signal_end.notify_all();
  return ret;
}

void counter::end() {
  to_do = false;
  signal_end.notify_all();
}

