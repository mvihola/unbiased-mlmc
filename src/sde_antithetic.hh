/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SDE_ANTITHETIC_HH_
#define _SDE_ANTITHETIC_HH_

#include <vector>
#include <random>
#include "sde.hh"

using namespace std;

/**
 * SDE class with antithetic truncated Milstein discretisation.
 */
class sde_antithetic : public sde {
 private:
   // The derivative function
   param_func dsigma;
   
   // Some temporary space
   double *x_, *x, *mu_x, *sigma_x, *dB, *dsigma_x, *dBdBT;

   void milstein_inc(double dt, double* y);
 public:
   void level(double* y, int n);
   void milstein_level(double* y, int n, int swap);
   
   // Default constructor: Use default values
   sde_antithetic(param_func mu, param_func sigma, 
                  param_func dsigma, target_func f,
       const vector<double> &x0 = vector<double>(1, 0.0), 
       double T = 1.0, int base_ = 2, 
       const vector<int> &active_levels_ = vector<int>(),
       int max_level = -1);
   
   // Copy constructor
   sde_antithetic(const sde_antithetic &other) :
   sde_antithetic(other.mu, other.sigma, other.dsigma, other.f, other.x0, 
                  other.T, other.base, other.active_levels, other.max_level) {};
   
   simulator* clone();
   ~sde_antithetic();

   // Must be redefined here
   virtual void level_diff(double* y, int n, int m);
   
   // Redefine this as well...
   virtual uint64_t cost(int n, int m);
};


#endif
