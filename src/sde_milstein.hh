/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SDE_MILSTEIN_HH_
#define _SDE_MILSTEIN_HH_

#include <vector>
#include <random>
#include "sde.hh"

using namespace std;

/**
 * SDE class with Milstein discretisation.
 */
class sde_milstein : public sde {
 private:
   // Derivative of diffusion function
   param_func dsigma;
   
 public:
   // Calculate simple euler discretisation
   void level(double* y, int n);
   
   // Default constructor: Use default values
   sde_milstein(param_func mu, param_func sigma, param_func dsigma,
                target_func f,
       const vector<double> &x0 = vector<double>(1, 0.0), 
       double T = 1.0, 
       int base = 2, const vector<int> &active_levels = vector<int>(),
       int max_level = -1);
   
   ~sde_milstein();
   
   // Copy constructor
   sde_milstein(const sde_milstein& other) :
   sde_milstein(other.mu, other.sigma, other.dsigma, other.f, other.x0, other.T, 
                other.base, other.active_levels, other.max_level) {};

   simulator* clone();
};

#endif
