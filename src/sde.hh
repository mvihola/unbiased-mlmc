/* -*- mode: C++; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _sde_HH_
#define _sde_HH_

#include <vector>
#include <random>
#include <cassert>

#include "types.hh"
#include "simulator.hh"

using namespace std;


/**
 * A virtual sde class, which implements common functionalities in
 * all SDE simulators.
 */
class sde : public simulator {
 protected:
   // The driving unit noise
   normal_distribution<double> randn;
  
   // Pointer to Brownian path data (allocated when needed)
   double* B;
   // How long Brownian path fits to B
   int nalloc;
   // The current number of points in the Brownian path
   int n_bm;  
   
   // Pointers to temporary state and parameter variables
   double *x_, *x, *mu_x, *sigma_x, *y_;
   
   // State dimension (default: 1)
   int d;
   // Initial state value (default: zeros)
   vector<double> x0;
   // Terminal time (initial time = 0)
   double T;

   // Drift function
   param_func mu;
   // Volatility function
   param_func sigma;
   
   // The target function
   target_func f;
   
   // The base of MLMC
   int base;
   
   // Helper function which just calculates base^i
   uint64_t base_n(int i);
   
   // Vector containing the active discretisation levels
   vector<int> active_levels;
   
   // The maximum level allowed
   int max_level;
   
   // Helper which maps level indicator to discretisation level
   int determine_level(int i);
   
   // Function which must calculate the value of a level n 
   // discretisation, assuming that at least level n BM has
   // been simulated before
   virtual void level(double* y, int n) = 0;
 public:
   // Default constructor: Use default values as stated above
   sde(target_func f, const vector<double> &x0 = vector<double>(1, 0.0), 
       double T = 1.0, int base = 2, 
       const vector<int> &active_levels = vector<int>(), 
       int max_level = -1);
   virtual ~sde();
   
   virtual simulator* clone() = 0;
   
   // Function which simulates a Brownian path of length of active level n
   virtual void simulate_bm(int n, PRNG& g);
   
   // Function which calculates a difference of two active levels
   // based on one Monte Carlo sample
   virtual void level_diff(double* y, int n, int m);

   virtual uint64_t cost(int n, int m);
   
   // Simple stuff getting parameters
   vector<double> get_x0() { return(x0); };
   int get_d() { return(f.d); };
   target_func get_f() { return(f); };
};

#endif
