#!/bin/sh

# This runs all tests in the paper.
# WARNING: this takes a while.
# Please set the "threads" parameter in "run_test.sh" 
# script properly.

for model in gbm_rg cir_rg heston_rg cubic; do
  for scheme in ml iid strat resid syst; do
      ./run_test_coupled.sh "${model}" "${scheme}"
  done
done
