#!/bin/sh

# This runs all tests in the paper.
# WARNING: this takes a while.
# Please set the "threads" parameter in "run_test.sh" 
# script properly.

for model in gbm_rg cir_rg heston_rg cubic; do
  for scheme in ml iid strat resid syst; do
    for sum in 0 1; do
      ./run_test_independent.sh "${model}" "${scheme}" "${sum}"  
    done
  done
done
