#!/bin/sh
#SBATCH -J run_test.sh
#SBATCH -o out/log_%j_output.txt
#SBATCH -e out/log_%j_errors.txt
#SBATCH -n 1
#SBATCH -t 06:00:00
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH -p serial

model="$1"
scheme="$2"

# This detects number of threads automatically:
threads="nil"
# Uncomment to determine the number of threads manually:
#threads="16"

config="models/${model}.config"

# Number of repetitions
batch="100e3"

for n in 1000 10000 100000 1000000; do
  outf="out/${model}_${scheme}_${n}_coupled"
  if [ -e "${outf}.bin" ]; then
    echo "Skipping existing ${outf}"
  else
    CONF="sampling_scheme=\"${scheme}\"; \
          out_file=\"${outf}.bin\"; binary=1; \
          n=$n; batch=${batch}; n_threads=${threads}; coupled=1; \
          p=p_coupled; tail_prob=tail_prob_coupled; levels=levels_coupled;"
    echo "Running ${outf}"
    ./src/unbiased $config -e "$CONF" -c "${outf}.config"
  fi
done
