This is Unbiased multilevel Monte Carlo (UMLMC) research software,
related to the research article "Unbiased estimators and multilevel
Monte Carlo", [Operations Research 66(2):448–462, 2018](https://doi.org/10.1287/opre.2017.1670); preprint [arXiv:1512.01022](https://arxiv.org/abs/1512.01022)

UMLMC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UMLMC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.


# Compiling

In order to compile UMLMC, you need a copy of Lua software library.

1. If Lua is preinstalled to your system, edit the Lua path in the
   top-level Makefile.in, where also other changes may be necessary.

2. If you have not installed Lua, and wish not to install it to your system, you may try the following (with Lua 5.3.3):
   1. Download Lua source package lua-5.3.3.tar.gz
      from https://www.lua.org/download.html
   2. Extract the source package to this directory.
```
      tar zxvf lua-5.3.3.tar.gz
```      
   3. Enter the following commands:
```
      cd lua-5.3.3
      make SYSTEM    (where SYSTEM = linux, macosx or sth else...)
      cd ..
```      
   4. Uncomment the following lines in the Makefile.in:
```
      LUA_INCLUDE=-I../lua-5.3.3/src/
      LUA_LIB=-L../lua-5.3.3/src -llua
```
3. Make both the main executable as well as the model libraries
   by typing `make`.

4. Test that the software works: type
```
   ./src/unbiased models/gbm_rg.config -e 'summary=1;batch=1'
```

# Tests

The tests in the paper can be repeated by running the following commands:
(Warning: these take a long time to run!)
```
  ./run_all_independent.sh
  ./run_all_coupled.sh
```
After this, the plot as in the paper can be produced by starting
R in this directory, and with the R command
```
  source("tools/plot_results.r")
```
This produces the picture in "all.pdf", and the results corresponding
to n=10^6 are in the matrix "finals"


The single term estimator can be optimised by the software, by running
with `optimise=1`, such as:
```
  src/unbiased models/cir_rg.config \
  -e 'p = p_[1]; tail_prob = tail_prob_[1]; optimise=1' \
  -c cir_rg_optimised.config
```
Here, the algorithm is run with certain parameters, but the
configuration saved in `cir_rg_optimised.config` contains the
optimised parameters


The coupled sum estimators can be optimised by running
```
  ./coupled_stats.sh
```
and then running in R
```
  source("tools/optimise_coupled.r")
```

# Contents

`src/`:
  The source code of the main software. Once compiled, the file `unbiased` is the executable.

`models/`:
  The source code of model libraries (.c) and corresponding configurations
  (.config). Once compiled, the dynamically linked libraries (.so).

`tools/`:
  Tools to read the data from the software to R and Matlab
  (`unbiased_read.r`, `unbiased_read.m`), the
  script which produces the picture in the final paper
  (`plot_results.r`), and the script which optimises
  the coupled sum estimators (`optimise_coupled.r`).
