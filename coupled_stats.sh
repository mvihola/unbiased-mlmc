#!/bin/sh

batch=1; n=1e2; ref_lev=13

for model in gbm_rg cir_rg heston_rg cubic; do
  for lev in $(seq -1 12); do
    config="models/${model}.config"
    outf="out/coupled_stats_${model}_level${lev}"
    CONF="sampling_scheme=\"ml\"; \
    stats_file=\"${outf}.bin\"; \
    n=${n}; batch=${batch}; sum=0; \
    levels={${lev},${ref_lev}}; 
    p={0,1}; tail_prob=0"
    echo "${outf}"
    ./src/unbiased $config -e "$CONF" -c "${outf}.config"
  done
done
