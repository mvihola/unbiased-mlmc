/* -*- mode: C; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>

#define MAX(X,Y) (((X) >= (Y)) ? (X) : (Y))

/* The CIR parameters */
#define CIR_A 5
#define CIR_B 0.04
#define CIR_SIGMA 0.25

void mu(const double *x, double *y) {
  y[0] = CIR_A*(CIR_B-x[0]);
}

void sigma(const double *x, double *y) {
  y[0] = CIR_SIGMA*sqrt(MAX(x[0], 0.0));
}

void dsigma(const double *x, double *y) {
  y[0] = .5*CIR_SIGMA/sqrt(MAX(x[0], 0.0));
}

// Objective function
void f(const double* x, double* y) {
  y[0] = exp(-0.05)*MAX(x[0]-0.03,0.0);
}
// (which has expectation ~0.0120124)

