/* -*- mode: C; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>

#define MAX(X,Y) (((X) >= (Y)) ? (X) : (Y))

const double SIGMA = 0.1;

// Drift
void mu(const double* x, double t, double* val) {
  val[0] = MAX(t*t*x[0], 0.0);
}

// Diffusion
void sigma(const double* x, double t, double* val) {
  val[0] = MAX(SIGMA*x[0], 0.0);
}

// Derivative of diffusion
void dsigma(const double* x, double t, double* val) {
  val[0] = (x[0]>0.00) ? SIGMA : 0.0;
}

// Objective function
void f(const double* x, double* y) {
  y[0] = x[0];
}

