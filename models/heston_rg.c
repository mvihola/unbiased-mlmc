/* -*- mode: C; mode: fold -*- */
/*
 * Copyright (c) Matti Vihola 2016
 * 
 * This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
 * software.
 * 
 * UMLMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * UMLMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>

#define D 2
#define MAX(X,Y) (((X) >= (Y)) ? (X) : (Y))
// Helper for indexing matrices
#define IND2(I,J) (I-1 + D*(J-1))
#define IND3(I,J,L) (I-1 + D*(J-1) + D*D*(L-1))

// This is the 2D Heston model used in Rhee & Glynn (2015)
const double HESTON_MU = 0.05;
const double HESTON_KAPPA = 5;
const double HESTON_THETA = 0.04;
const double HESTON_SIGMA = 0.25;
const double HESTON_RHO = -0.5;

// Drift: R^2 -> R^2
void mu(const double* x, double t, double* val) {
  val[0] = HESTON_MU*x[0];
  val[1] = HESTON_KAPPA*(HESTON_THETA-x[1]);
}

// Diffusion: R^2 -> R^(2x2)
void sigma(const double* x, double t, double* val) {
  double sqrtv = sqrt(MAX(x[1], 0.0));
  double sc0 = sqrtv*x[0];
  double sc1 = HESTON_SIGMA*sqrtv;
  val[IND2(1,1)] = 1.0 * sc0;
  val[IND2(2,1)] = HESTON_RHO * sc1; 
  val[IND2(1,2)] = 0.0;
  val[IND2(2,2)] = sqrt(1.0-HESTON_RHO*HESTON_RHO) * sc1;
}

// Objective function: R^2 -> R^(2x2x2)
// If S(x) is the outcome of sigma(x), then
// val[IND3(i,j,k)] is the d S(x)/d x_k.
void dsigma(const double* x, double t, double* val) {
  double sqrtv = sqrt(MAX(x[1], 0.0));
  double sqrtv_d2 = .5/sqrt(MAX(x[1], 0.0));
  double sc1_d2 = HESTON_SIGMA*sqrtv_d2;
  val[IND3(1,1,1)] = sqrtv;
  val[IND3(1,1,2)] = x[0]*sqrtv_d2;
  val[IND3(2,1,1)] = 0.0;
  val[IND3(2,1,2)] = HESTON_RHO * sc1_d2;
  val[IND3(1,2,1)] = 0.0;
  val[IND3(1,2,2)] = 0.0;
  val[IND3(2,2,1)] = 0.0;
  val[IND3(2,2,2)] = sqrt(1.0-HESTON_RHO*HESTON_RHO) * sc1_d2;
}

void f(const double* x, double* y) {
  double x_ = x[0]; 
  y[0] = exp(-HESTON_MU)*MAX(x_-1.0,0.0);
}
// (which has expectation ~0:10459672 )

