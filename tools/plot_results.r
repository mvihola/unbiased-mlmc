# Copyright (c) Matti Vihola 2016
# 
# This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
# software.
# 
# UMLMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# UMLMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.

#library(extrafont)
source("tools/unbiased_read.r")

plot_ec <- function(base, true_f, xaxt=FALSE, leg=FALSE,
                    show.title=FALSE, name="") {

scheme = c("ml", "iid", "strat", "resid", "syst")
scheme_plot = c("ML", "iid", "str", "res", "sys")
n = c(1e3, 10e3, 100e3, 1e6)
n_lab = c(expression(10^3), expression(10^4), expression(10^5),
expression(10^6))
n_n = length(n)
n_s = length(scheme)

ec = array(0, dim=c(n_n,n_s,3))

singlesum=c("0","1","coupled")
title.str=c("Single term","Independent sum","Coupled sum")

pch = c(4,1,3,2,5)
lty = c(3,1,4,2,5)
col = c(1,1,1,1,1)

for (s in 1:3) {
  for (i in 1:length(scheme)) {
    for (j in 1:length(n)) {
      str = sprintf("%s_%s_%i_%s.bin", base, scheme[i], n[j], singlesum[s])
      if (file.exists(str)) {
        a = unbiased_read(str)
      } else {
        sprintf("Skipped non-existing: %s", str)
      }
      ec_ = mean(a$c)*mean((a$m_1-true_f)^2)
      ec[j,i,s] = ec_
    }
  }
  ec_max = max(ec[,,s])
  ec_min = min(ec[,,s])
  delta = 0.025*diff(range(ec[,,s]))
  ylim = c(ec_min-delta,ec_max+delta)
  par(las=1)
  matplot(n, ec[,,s], ylim=ylim, log="x", type="b", xaxt="n", yaxt="n",
  pch=pch, lty=lty, col=col, lwd=1)
  if (show.title) { mtext(title.str[s], side=3) }
  axis(2, tcl=-0.3, hadj=0.7)
  if (xaxt) { axis(1, at=n , labels=n_lab)}
  if (s==3) { par(las=3); mtext(name, 4) }
}

if (leg) {
  legend("right", legend = scheme_plot, col=col, y.intersp = 0.7,
          pch=pch, lty=lty,lwd=1, bty="n" )
}

ec[length(n),,]
}

open_pdf <- function(fname, w=4, h=2, margins_reduce=c(3,0,3.5,0)) {
  pdf(fname, onefile = FALSE, paper =
  "special", width=w, height=h, family="Times")
  par(ps=9)
  par(mar=par()$mar-margins_reduce)
  # Margins: bottom, left, top, right
}

open_pdf("all.pdf", h=5,w=6.5)

par(mfrow=c(4,3), cex=1, mar=c(0.5,2.4,0,0.5), oma=c(2,0.3,0.75,0.5))

finals = plot_ec("out/gbm_rg", 0.104505836, FALSE, FALSE, TRUE, "gBM")
finals_ = plot_ec("out/cir_rg", 0.0114268648028, FALSE, FALSE, FALSE, "CIR")
finals = cbind(finals, finals_)
finals_ = plot_ec("out/heston_rg",0.10459672, FALSE, FALSE, FALSE, "Heston")
finals = cbind(finals, finals_)
finals_ = plot_ec("out/cubic", 1.39561213944, TRUE, TRUE, FALSE, "Modified gBM")
finals = cbind(finals, finals_)

dev.off()
