# Copyright (c) Matti Vihola 2016
# 
# This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
# software.
# 
# UMLMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# UMLMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.

unbiased_read <- function(fname) {
  
  if (!file.exists(fname)) 
    stop(c("Cannot open file '", fname ,"'"))
  
  ifh <- file(fname, "rb")
   
  # Read the header line
  hdr <- readLines(ifh, n=1)
  hdr_ <- strsplit(hdr, ",", fixed=TRUE)
  Nvars <- length(hdr_[[1]])
  
  # Make sure that we read complete records, and that thinning
  # is done at equal steps
  nblock <- Nvars*2^20
  
  # Initialise the output variable
  data = matrix(nrow=0,ncol=Nvars)
  colnames(data) <- hdr_[[1]]
  
  # Read data block by block
  repeat {
    dd <-readBin(ifh, "double", n=nblock)
    # Check whether there was enough data
    Ndd <- length(dd)
    if (Ndd==0) {
      break 
    } else if ((Ndd %% Nvars) != 0) {
      print("Warning: last record was broken!")
    }
    ddm <- matrix(dd, ncol=Nvars, byrow=TRUE);
    data <- rbind(data, ddm)
  }
  
  close(ifh)
  
  return(as.data.frame(data))
}

unbiased_stats_read <- function(fname, d=1) {
  # You must pass the dimension of the function unless one
  stopifnot(file.exists(fname))
  
  means = list(0)
  vars = list(0)
  n_lev = list(0)
  m = numeric(0)
  
  ifh <- file(fname, "rb") 
  # Read the header line
  hdr_ <- readLines(ifh, n=1)
  i = 1
  repeat {
    ## Read cost and m
    m_ <- readBin(ifh, "integer", n=1, size=8);
    if (length(m_)!=1) { break }
    m <- c(m,m_)
    n_lev[[i]] <- readBin(ifh, "integer", n=m_, size=8)
    means[[i]] <- readBin(ifh, "double", n=m_*d)
    vars[[i]] <- readBin(ifh, "double", n=m_*d)
    i = i+1
  }
  close(ifh)
  list(m=means, v=vars, n=n_lev, M=m)
}
