# Copyright (c) Matti Vihola 2016
# 
# This file is part of Unbiased multilevel Monte Carlo (UMLMC) research 
# software.
# 
# UMLMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# UMLMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with UMLMC.  If not, see <http://www.gnu.org/licenses/>.


# This should be run after running "coupled_stats.sh":
# Run R at top level, and the commands:
# source("tools/optimise_coupled.r")

source("tools/unbiased_read.r")

# The highest level used as an approximation of Y
high_lev <- 13

# Maximum level for which diffs are calculated
max_lev <- 12

# All models
models <- c("gbm_rg", "cir_rg", "heston_rg", "cubic")

# Initialise variance and cost matrices
v <- matrix(0, max_lev+2, length(models)) 
colnames(v) <- models
cost <- v

for (m in 1:length(models)) {
for (lev in -1:max_lev) {
  fname <- sprintf("out/coupled_stats_%s_level%i.bin", models[m], lev)
  a <- unbiased_stats_read(fname)
  v[lev+2,m] <- a$v[[1]][2]
  cost[lev+2,m] <- 2^(lev+1)
}
}
cost <- as.data.frame(cost)
v <- as.data.frame(v)

optimise.coupled <- function(cost, v, n=1, convexify=T) {
  # cost[i] \propto cost of level i
  # v[i] = var(Y_i-Y)
  # n is the number of custom points (not taking negative values  into
  # account)
  
  # First get rid of negative values:
  beta <- I <- vector(mode="numeric", length=0)
  prev <- 1
  for (k in 2:length(v)) {
    if (v[k]<v[prev]) {
      I <- c(I, k)
      beta <- c(beta, v[prev]-v[k])
      prev <- k
    }
  }
  # Length of the vector of diffs, when negative values are omitted
  N <- length(beta)

  # Check that we are fine...
  if (n>= N-1) stop("n must be smaller than number of positive differences")
  
  # Check that no negative indices beyond n:
  if (any(diff(I[(n+1):N]) != 1)) {
    stop("All differences must be positive beyond n")
  }
  
  # Calculate cost of the level differences:
  t <- colSums(rbind(cost[I], cost[I-1]))
  
  # Find optimal "convexification" subsequence (up to n)
  if (n>1 && convexify) {
    J_ <- optimal.subsequence(beta, t, m=n)
  } else {
    J_ <- 1:n
  }
  n_J <- length(J_)
  J_ <- c(J_, (n+1):N)
  
  # J stands for the real indices, when also negative guys are dropped out
  J <- I[J_]-1
  N_J <- length(J)
  
  # Construct beta_J and t_J sequences
  beta_J <- t_J <- vector("numeric", length=N_J)
  for (i in 1:N_J) {
    beta_ <- 0; t_ <- 0
    if (i == N_J) {
      term_ind <- N
    } else {
      term_ind <- J_[i+1]-1
    }
    for (j in J_[i]:term_ind) { 
      beta_ <- beta_ + beta[j] 
      t_ <- t_ + t[j]
    }
    beta_J[i] <- beta_; 
    t_J[i] <- t_;
  }
  
  # Optimal (candidate) "reverse cdf" F[i] = \sum_{j\ge i} p[j]:
  F <- sqrt(beta_J/t_J)
  F <- F/F[1]
  
  # One more sanity check: this must define a distribution:
  if (any(diff(F)>0)) {
    stop("The final sequence not convex! Try bigger n?")
  }
  
  # If this is true, then just find the pmf corresp. F:
  p_ <- -diff(F)
  
  # The custom part:
  p <- p_[1:n_J]
  
  # The tail part:
  p_tail <- p_[(n_J+1):(N_J-1)]
  tail_prob <- F[n_J+1]
  
  # Just do simple indices:
  ind <- (n_J+1):(N_J-1)
  
  # Take base-2 log of the tail probabilities
  y <- log2(p_tail)
  
  # Fit a line; gamma is the slope
  fit <- lm(y ~ ind+1)
  gamma <- as.vector(-fit$coefficients[2])
  
  # Plot everybody:
  suppressWarnings(plot(J[1:(N_J-1)], p_, log="y", col="black"))
  
  # Custom ones in blue:
  points(J[1:n_J], p, col="blue", pch="*")
  
  # Reconstruct "reverse cdf" 
  pp_tail <- (1-2^(-gamma))*2^(-gamma*(J[(n_J+1):(N_J)]-J[n_J+1]))
  pp <- c((1-tail_prob)*p/sum(p), tail_prob*pp_tail)
  FF <- rev(cumsum(rev(pp)))
  opt_val <- sum(beta_J/FF)*sum(t_J*FF)
  
  # Draw fit
  lines(J[(n_J+1):(N_J)], tail_prob*pp_tail, col="red", type="l")

  list(p=p, tail_prob=tail_prob, gamma=gamma, levels=J[1:n_J]-1,
              value=opt_val)
}


# This implements Algorithm 1 of Rhee & Glynn (2015)
# with essentially identical notation
optimal.subsequence <- function(beta, t, m=length(beta)) {  

  stopifnot(m <= length(t) && m <= length(beta))
  
  L <- matrix(0, m, m)
  # Calculate values in L matrix
  for (k in 1:m) {
    for (l in k:m) {
      L[k,l] <- sum(beta[k:l])/sum(t[k:l])
    }
  }

  G <- matrix(0, m, m)
  J <- matrix(rep(list(), m*m), m, m)
  J[[1,1]] <- 1
  G[1,1] <- sqrt(beta[1]*t[1])
  for (l in 2:m) {
    J[[1,l]] <- 1
    G[1,l] <- sqrt(sum(beta[1:l])*sum(t[1:l]))
    for (k in 2:l) {
      J[[k,l]] <- vector(mode="numeric", length=0)
      G[k,l] <- Inf
      P <- sqrt(sum(beta[k:l])*sum(t[k:l]))
      for (i in 1:(k-1)) {
        if (length(J[i,k-1]) > 0) {
          J_ <- union(J[[i,k-1]], k)
          G_ <- G[i,k-1] + P
          if (L[i,k-1] > L[k,l] && G_ < G[k,l]) {
            J[[k,l]] <- J_
            G[k,l] <- G_
          }
        }
      }
    }
  }
  k_ <- which.min(G[,m])
  J[[k_,m]]
}


print(optimise.coupled(cost$gbm_rg, v$gbm_rg, n=1))
readline(prompt="Press [enter] to continue")
print(optimise.coupled(cost$cir_rg, v$cir_rg, n=3))
readline(prompt="Press [enter] to continue")
print(optimise.coupled(cost$heston_rg, v$heston_rg, n=3))
readline(prompt="Press [enter] to continue")
print(optimise.coupled(cost$cubic, v$cubic, n=4))
readline(prompt="Press [enter] to continue")
