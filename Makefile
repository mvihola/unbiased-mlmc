.PHONY: models src all clean clean-models clean-src pkg

all: models src

models:
	cd models && make

src:
	cd src && make

clean: clean-models clean-src

clean-src:
	cd src && make clean

clean-models:
	cd models && make clean

pkg: clean
	tar zcvf umlmc-$(shell date +"%Y-%m-%d").tar.gz --no-recursion \
  src/Makefile src/*.cc src/*.hh models/Makefile \
  models  tools \
  run_*.sh \
  out Makefile Makefile.in COPYING README

